 // EDITAR CADASTRO DE CLIENTES
        function editarcadcli (id){
            
            var config = {
                url: "grid_cliente.php",
                method: "POST",
                data: {
                    operacao: "carregar",
                    id:id
                },
                dataType : "json",
                error : function(x){
                console.log(x);
                },
                success: function (x){
                console.log(x);
                $("#id").val(x.id);
                $("#cnpj").val(x.cnpj);
                $("#razao").val(x.razao);
                $("#fantasia").val(x.fantasia);
                $("#insc").val(x.insc);
                $("#cep").val(x.cep);
                $("#endereco").val(x.endereco);
                $("#bairro").val(x.bairro);
                $("#cidade").val(x.cidade);
                $("#uf").val(x.uf);
                $("#compl").val(x.compl);
                $("#email").val(x.email);
                $("#cpf").val(x.cpf);
                $("#telefone").val(x.telefone);
                $("#celular").val(x.celular);
                $("#pessoa").val(x.pessoa);
                $("#contato").val(x.contato);
                $("#obs").val(x.obs);
                $("#operacao").val("editar");
                $("#abremodal").modal("show");
                remaskcli();
                }

            }
            console.log(config);
            $.ajax(config)
        }

        function remaskcli (){
                $('#cnpj').unmask().mask('00.000.000/0000-00', {reverse: true});
                $('#cpf').unmask().mask('000.000.000-00', {reverse: true});
                $('#cep').unmask().mask('00000-000');
                $('#telefone').unmask().mask('(00) 0000-0000');
                $('#celular').unmask().mask('(00) 00000-0000');
               // alert('Ativou!');
        }

// EDITAR CADASTRO DE FUNCIONARIOS
        function editarcadfunc (id){
            
            var config = {
                url: "grid_funcionarios.php",
                method: "POST",
                data: {
                    operacao: "carregar",
                    id:id
                },
                dataType : "json",
                error : function(x){
                console.log(x);
                },
                success: function (x){
                console.log(x);
                $("#id").val(x.id);
                $("#nome").val(x.nome);
                $("#apelido").val(x.apelido);
                $("#telefone").val(x.telefone);
                $("#celular").val(x.celular);
                $("#setor").val(x.setor);
                $("#funcao").val(x.funcao);
                $("#cep").val(x.cep);
                $("#endereco").val(x.endereco);
                $("#bairro").val(x.bairro);
                $("#cidade").val(x.cidade);
                $("#uf").val(x.uf);
                $("#status").val(x.status);
                $("#email").val(x.email);
                $("#cpf").val(x.cpf);
                $("#obs").val(x.obs);
                $("#sup").val(x.tecnico);
                $("#dev").val(x.dev);
                $("#operacao").val("editar");
                $("#abremodal").modal("show");
                remaskfunc();
                }

            }
            console.log(config);
            $.ajax(config)
        }
         function remaskfunc (){
                $('#cpf').unmask().mask('000.000.000-00', {reverse: true});
                $('#cep').unmask().mask('00000-000');
                $('#telefone').unmask().mask('(00) 0000-0000');
                $('#celular').unmask().mask('(00) 00000-0000');
               // alert('Ativou!');
        }   

 // EDITAR CADASTRO DE USUARIOS
        function editarcaduser (id){
            
            var config = {
                url: "grid_cad_usr.php",
                method: "POST",
                data: {
                    operacao: "carregar",
                    id:id
                },
                dataType : "json",
                error : function(x){
                console.log(x);
                },
                success: function (x){
                console.log(x);
                $("#id").val(x.id);
                $("#login").val(x.login);
                $("#senha").val();
                $("#nivel").val(x.nivel);
                $("#func").val(x.funcionario);
                $("#operacao").val("editar");
                $("#abremodal").modal("show");
                }

            }
            console.log(config);
            $.ajax(config)
        } 

 // EDITAR CADASTRO DE NIVEIS
        function editarcadnivel (id){
            
            var config = {
                url: "grid_niveis.php",
                method: "POST",
                data: {
                    operacao: "carregar",
                    id:id
                },
                dataType : "json",
                error : function(x){
                console.log(x);
                },
                success: function (x){
                console.log(x);
                $("#id").val(x.id);
                $("#descricao").val(x.descricao);
                $("#cadcli").val(x.cadcli);
                $("#cadfunc").val(x.cadfunc);
                $("#cadsist").val(x.cadsist);
                $("#cadnivel").val(x.cadnivel);
                $("#cadativ").val(x.cadativ);
                $("#cadusr").val(x.cadusr);
                $("#cadservtec").val(x.cadservtec);
                $("#cadatendtec").val(x.cadatendtec);
                $("#atendsup").val(x.atendsup);
                $("#atenddev").val(x.atenddev);
                $("#treina").val(x.treina);
                $("#implantacao").val(x.implantacao);
                $("#cadsis").val(x.cadsis);
                $("#conscnpj").val(x.conscnpj);
                $("#senhasup").val(x.senhasup);
                $("#operacao").val("editar");
                $("#abremodal").modal("show");
                }

            }
            console.log(config);
            $.ajax(config)
        }                     

        //EDITAR CHAMADOS SUPORTE
        function editarcadchamsup (id){
            
            var config = {
                url: "movim_suporte.php",
                method: "POST",
                data: {
                    operacao: "carregar",
                    id:id
                },
                dataType : "json",
                error : function(x){
                console.log(x);
                },
                success: function (x){
                console.log(x);
                $("#id").val(x.id);
                $("#data").val(x.dt_abertura);
                $("#cliente").val(x.cliente).trigger("change");
                $("#tecnico").val(x.tecnico);
                $("#servico").val(x.servico);
                $("#tipoatend").val(x.tipo_atend);
                $("#sistema").val(x.sistema);
                $("#contato").val(x.contato);
                $("#tpentrada").val(x.tipo_entrada);
                $("#setor").val(x.setor);
                $("#descricao").val(x.descr_chamado);
                $("#operacao").val("editar");
                $("#abremodal").modal("show");
                }

            }
            console.log(config);
            $.ajax(config)
        }     
  // Ver chamados suporte             
     function verchamadosup (id){
            
            var config = {
                url: "movim_suporte.php",
                method: "POST",
                data: {
                    operacao: "carregar",
                    id:id
                },
                dataType : "json",
                error : function(x){
                console.log(x);
                },
                success: function (x){
                console.log(x);
                $("#verid").val(x.id);
                $("#verdata").val(x.dt_abertura);
                $("#vercliente").val(x.cliente);
                $("#vertecnico").val(x.tecnico);
                $("#verservico").val(x.servico);
                $("#vertipoatend").val(x.tipo_atend);
                $("#versistema").val(x.sistema);
                $("#vercontato").val(x.contato);
                $("#vertpentrada").val(x.tipo_entrada);
                $("#versetor").val(x.setor);
                $("#verdescricao").val(x.descr_chamado);
                $("#verdetalhetec").val(x.descr_tec);
                $("#operacao").val("editar");
                $("#vermodal").modal("show");
                }

            }
            console.log(config);
            $.ajax(config)
        }            
  // Ver Atender suporte
      function atendechamadosup (id){
            
            var config = {
                url: "movim_suporte.php",
                method: "POST",
                data: {
                    operacao: "carregar",
                    id:id
                },
                dataType : "json",
                error : function(x){
                console.log(x);
                },
                success: function (x){
                console.log(x);
                $("#atid").val(x.id);
                $("#atdata").val(x.dt_abertura);
                $("#atcliente").val(x.cliente);
                $("#attecnico").val(x.tecnico);
                $("#atservico").val(x.servico);
                $("#attipoatend").val(x.tipo_atend);
                $("#atsistema").val(x.sistema);
                $("#atcontato").val(x.contato);
                $("#attpentrada").val(x.tipo_entrada);
                $("#atsetor").val(x.setor);
                $("#atdescricao").val(x.descr_chamado);
                $("#detalhetec").val(x.descr_tec);
                $("#operacao").val("editar");
                $("#operacao1").val();
                $("#acionardev").val(x.id);
                $("#atende").modal("show");
                }

            }
            console.log(config);
            $.ajax(config)
        }           

  function funcoperacao (op){

     $("#operacao1").val(op);
    // alert( document.getElementById('operacao').value);
     //console.log( $("#operacao").val());
     document.getElementById('operacao1').value=op;
    // alert( document.getElementById('operacao').value);

  }             

 // Atualiza status inicial OS SUPORTE
 function statusinicial (id){
            
            var config = {
                url: "movim_suporte.php",
                method: "POST",
                data: {
                    operacao: "atualizainicial",
                    id:id
                },
                dataType : "json",
                error : function(x){
                console.log(x);
                },
                success: function (x){
                console.log(x);
                $("#idinsert").val(x.id);
                $("#operacao").val("atualizainicial");

                }

            }
            console.log(config);
            $.ajax(config)
        }                    