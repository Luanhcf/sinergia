
        //EDITAR CHAMADOS SUPORTE
        function editarcadchamsup (id){
            
            var config = {
                url: "movim_desenvolvimento.php",
                method: "POST",
                data: {
                    operacao: "carregar",
                    id:id
                },
                dataType : "json",
                error : function(x){
                console.log(x);
                },
                success: function (x){
                console.log(x);
                $("#id").val(x.id);
                $("#data").val(x.dt_abertura);
                $("#cliente").val(x.cliente);
                $("#desenv").val(x.desenv);
                $("#modulo").val(x.modulo);
                $("#prioridade").val(x.prioridade);
                $("#sistema").val(x.sistema);
                $("#sprint").val(x.sprint);
                $("#setor").val(x.setor);
                $("#descricao").val(x.descr_chamado);
                $("#operacao").val("editar");
                $("#abremodal").modal("show");
                }

            }
            console.log(config);
            $.ajax(config)
        }     
  // Ver chamados suporte             
     function verchamadosup (id){
            
            var config = {
                url: "movim_desenvolvimento.php",
                method: "POST",
                data: {
                    operacao: "carregar",
                    id:id
                },
                dataType : "json",
                error : function(x){
                console.log(x);
                },
                success: function (x){
                console.log(x);
                $("#verid").val(x.id);
                $("#verdata").val(x.dt_abertura);
                $("#vercliente").val(x.cliente);
                $("#verdesenv").val(x.desenv);
                $("#vermodulo").val(x.modulo);
                $("#verprioridade").val(x.prioridade);
                $("#versistema").val(x.sistema);
                $("#versprint").val(x.sprint);
                $("#versetor").val(x.setor);
                $("#vertecnico").val(x.tecnico);
                $("#verdescricao").val(x.descr_chamado);
                $("#verdetalhetec").val(x.descr_tec);
                $("#verdetalhedev").val(x.descr_dev);
                $("#operacao").val("editar");
                $("#vermodal").modal("show");
                }

            }
            console.log(config);
            $.ajax(config)
        }            
  // Ver Atender suporte
      function atendechamadosup (id){
            
            var config = {
                url: "movim_desenvolvimento.php",
                method: "POST",
                data: {
                    operacao: "carregar",
                    id:id
                },
                dataType : "json",
                error : function(x){
                console.log(x);
                },
                success: function (x){
                console.log(x);
                $("#atid").val(x.id);
                $("#atdata").val(x.dt_abertura);
                $("#atcliente").val(x.cliente);
                $("#attecnico").val(x.tecnico);
                $("#atdesenv").val(x.desenv);
                $("#atmodulo").val(x.modulo);
                $("#attipoatend").val(x.tipo_atend);
                $("#atsistema").val(x.sistema);
                $("#atcontato").val(x.contato);
                $("#attpentrada").val(x.tipo_entrada);
                $("#atsetor").val(x.setor);
                $("#atdescricao").val(x.descr_chamado);
                $("#atprioridade").val(x.prioridade);
                $("#atsprint").val(x.sprint);
                $("#descr_dev").val(x.descr_dev);
                $("#detalhetec").val(x.descr_tec);
                $("#operacao").val("editar");
                $("#operacao1").val();
                $("#acionardev").val(x.id);
                $("#atende").modal("show");
                }

            }
            console.log(config);
            $.ajax(config)
        }           

  function funcoperacao (op){

     $("#operacao1").val(op);
    // alert( document.getElementById('operacao').value);
     //console.log( $("#operacao").val());
     document.getElementById('operacao1').value=op;
    // alert( document.getElementById('operacao').value);

  }             

 // Atualiza status inicial OS SUPORTE
 function statusinicial (id){
            
            var config = {
                url: "movim_desenvolvimento.php",
                method: "POST",
                data: {
                    operacao: "atualizainicial",
                    id:id
                },
                dataType : "json",
                error : function(x){
                console.log(x);
                },
                success: function (x){
                console.log(x);
                $("#idinsert").val(x.id);
                $("#operacao").val("atualizainicial");

                }

            }
            console.log(config);
            $.ajax(config)
        }                    