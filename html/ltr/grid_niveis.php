<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:authentication-login.php");
  exit;
}
require_once('src/menu.php');
require_once ('src/conexao.php');
$operacao = "";
$operacao = isset($_POST['operacao']) ? $_POST['operacao'] : '';
$alert = isset($_GET['alert']) ? $_GET['alert'] : '';
$msg="";
if ($alert==1){
$msg="<div style=\"text-align:center\" class=\"alert alert-success\" role=\"alert\">
  Cadastro realizado com sucesso!
</div>";
}

// VARIAVEIS DA PAGINA
$descricao  = "";
$cadcli     = "";
$cadfunc    = "";
$cadsist    = "";
$cadnivel   = "";
$cadativ    = "";
$cadusr     = "";
$cadservtec = "";
$cadatendtec= "";
$atendsup   = "";
$atenddev   = "";
$treina     = "";
$implantacao= "";
$conscnpj   = "";
$senhasup   = "";




 if ($operacao == "novo"){

$descricao     = isset($_POST['descricao']) ? $_POST['descricao'] : '';
$cadcli        = isset($_POST['cadcli']) ? $_POST['cadcli'] : '';
$cadfunc       = isset($_POST['cadfunc']) ? $_POST['cadfunc'] : '';
$cadsist        = isset($_POST['cadsist']) ? $_POST['cadsist'] : '';
$cadnivel      = isset($_POST['cadnivel']) ? $_POST['cadnivel'] : '';
$cadativ       = isset($_POST['cadativ']) ? $_POST['cadativ'] : '';
$cadusr        = isset($_POST['cadusr']) ? $_POST['cadusr'] : '';
$cadservtec    = isset($_POST['cadservtec']) ? $_POST['cadservtec'] : '';
$cadatendtec   = isset($_POST['cadatendtec']) ? $_POST['cadatendtec'] : '';
$atendsup      = isset($_POST['atendsup']) ? $_POST['atendsup'] : '';
$atenddev      = isset($_POST['atenddev']) ? $_POST['atenddev'] : '';
$treina        = isset($_POST['treina']) ? $_POST['treina'] : '';
$implantacao   = isset($_POST['implantacao']) ? $_POST['implantacao'] : '';
$conscnpj      = isset($_POST['conscnpj']) ? $_POST['conscnpj'] : '';
$senhasup      = isset($_POST['senhasup']) ? $_POST['senhasup'] : '';


$sql = "insert into niveis (descricao,cadcli,cadfunc,cadusr,cadativ,cadsist,cadservtec,cadatendtec,atendsup,atenddev,treina,implantacao,conscnpj,senhasup,cadnivel)
values ('$descricao','$cadcli','$cadfunc','$cadusr','$cadativ','$cadsist','$cadservtec','$cadatendtec','$atendsup','$atenddev','$treina','$implantacao','$conscnpj','$senhasup','$cadnivel')";


 $exec = pg_exec($conexao,$sql); 
  header ("location:grid_niveis.php?alert=1");

 }
if ($operacao == "editar"){
$id     = isset($_POST['id']) ? $_POST['id'] : '';

$descricao     = isset($_POST['descricao']) ? $_POST['descricao'] : '';
$cadcli        = isset($_POST['cadcli']) ? $_POST['cadcli'] : '';
$cadfunc       = isset($_POST['cadfunc']) ? $_POST['cadfunc'] : '';
$cadsis        = isset($_POST['cadsis']) ? $_POST['cadsis'] : '';
$cadnivel      = isset($_POST['cadnivel']) ? $_POST['cadnivel'] : '';
$cadativ       = isset($_POST['cadativ']) ? $_POST['cadativ'] : '';
$cadusr        = isset($_POST['cadusr']) ? $_POST['cadusr'] : '';
$cadservtec    = isset($_POST['cadservtec']) ? $_POST['cadservtec'] : '';
$cadatendtec   = isset($_POST['cadatendtec']) ? $_POST['cadatendtec'] : '';
$atendsup      = isset($_POST['atendsup']) ? $_POST['atendsup'] : '';
$atenddev      = isset($_POST['atenddev']) ? $_POST['atenddev'] : '';
$treina        = isset($_POST['treina']) ? $_POST['treina'] : '';
$implantacao   = isset($_POST['implantacao']) ? $_POST['implantacao'] : '';
$conscnpj      = isset($_POST['conscnpj']) ? $_POST['conscnpj'] : '';
$senhasup      = isset($_POST['senhasup']) ? $_POST['senhasup'] : '';

$sql = "update niveis set descricao='$descricao',cadcli='$cadcli',cadfunc='$cadfunc',cadusr='$cadusr',cadativ='$cadativ',cadsist='$cadsist',cadservtec='$cadservtec',cadatendtec='$cadatendtec',
atendsup='$atendsup',atenddev='$atenddev',treina='$treina',implantacao='$implantacao',conscnpj='$conscnpj',senhasup='$senhasup',cadnivel='$cadnivel' where id=$id";

 $exec = pg_exec($conexao,$sql); 

}

if ($operacao == "carregar"){

$id  = isset($_POST['id']) ? $_POST['id'] : '';
$ret = "select * From niveis where id=$id";
$sqlrg = pg_query($conexao,$ret);
$res = pg_fetch_assoc($sqlrg);


print(json_encode($res));
 
 exit;

}

// CARREGANDO GRID PRINCIPAL
$exibegrid = "";
$sqlg = "select * from niveis";
$sqlrg = pg_query($conexao,$sqlg); 

while ($row=pg_fetch_assoc($sqlrg)){
   $exibegrid.= "<tr>
                <td><a href=\"#\" onclick=\"editarcadnivel(".$row["id"].")\">".$row["id"]."</a></td>
                <td>".$row["descricao"]."</td>
                </tr>";
}

?>



<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <?php echo ($title); ?>
    <!-- Custom CSS -->
    <link href="../../assets/libs/flot/css/float-chart.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <style>
     .divide {
        display:grid;
        grid-row : 1;
        grid-template-columns: 50% 50%;

     }   
    </style>

</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">
        <!-- MENU LATERAL A BUSCA SUPERIOR -->
         <?php echo ($header);
               echo ($mlateral); 
         ?>

<div class="page-wrapper">
    <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Cadastro de Niveis</h5>
                        <div class="table-responsive">
                            <div style="text-align:center">
             <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target=".bd-example-modal-lg">Inserir</button>
                                </div>
                <?php echo($msg); ?>
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    
                                    <tr>
                                        <th></th>
                                        <th>Nivel</th>
                                    </tr>

                                </thead>
                                <tbody>
                                <?php
                                echo ($exibegrid);
                                ?>   

                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>
 <!--DESENHANDO NOVO CLIENTE MODAL -->
 <form class="form-horizontal" method="POST" action="grid_niveis.php" target="_self">
 <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="abremodal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--CONTEUDO -->
<br>   
<h4 class="card-title">Cadastro de Niveis</h4>     
<div class="card-body">
    <div class="form-group row" style="text-align: center;">
        <div class="col-sm-9">
            <input type="hidden" class="form-control col-sm-2" name="id" id="id" readonly="true">
        </div>
    </div>
    <div class="form-group row" style="width: 80%">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Descrição</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="descricao" id="descricao">
        </div>
    </div>
    <h6 style="text-align: center;color:red;">Controle de Acessos "Cadastros"</h6>
    <br>
    <!-- INICIO DO BLOCO DE CADASTROS -->
    <div class="divide" style="text-align: center;">
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Cad.Cliente</label>
        <div class="col-sm-9">
            <select name="cadcli" id="cadcli" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Cad.Funcionários</label>
        <div class="col-sm-9">
            <select name="cadfunc" id="cadfunc" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Cad.Sistemas</label>
        <div class="col-sm-9">
            <select name="cadsist" id="cadsist" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Cad.Niveis</label>
        <div class="col-sm-9">
            <select name="cadnivel" id="cadnivel" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div>    
    <div class="form-group row">
        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Cad.Atividade</label>
        <div class="col-sm-9">
            <select name="cadativ" id="cadativ" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Cad.Usuários</label>
        <div class="col-sm-9">
            <select name="cadusr" id="cadusr" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Cad.Serv.Tec</label>
        <div class="col-sm-9">
            <select name="cadservtec" id="cadservtec" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Cad.Tipo.Atend</label>
        <div class="col-sm-9">
            <select name="cadatendtec" id="cadatendtec" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div>  
  </div>
  <!-- FIM DO BLOCO DE CADASTRO -->
<!-- INICIO DO BLOCO DE MOVIMENTACAO -->
  <h6 style="text-align: center;color:red;">Controle de Acessos "Movimentações"</h6>
  <br>
  <div class="divide" style="text-align: center;">
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Atend.Suporte</label>
        <div class="col-sm-9">
            <select name="atendsup" id="atendsup" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Cham.Desenvolvimento</label>
        <div class="col-sm-9">
            <select name="atenddev" id="atenddev" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Treinamentos</label>
        <div class="col-sm-9">
            <select name="treina" id="treina" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div>   
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Implantação</label>
        <div class="col-sm-9">
            <select name="implantacao" id="implantacao" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div> 
</div>
 <!-- FIM DO BLOCO DE CADASTRO -->
<!-- INICIO DO BLOCO DE MOVIMENTACAO -->
  <h6 style="text-align: center;color:red;">Controle de Acessos "Auxiliares"</h6>
  <br>
  <div class="divide" style="text-align: center;">
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Consulta.CNPJ</label>
        <div class="col-sm-9">
            <select name="conscnpj" id="conscnpj" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Senha.Suporte</label>
        <div class="col-sm-9">
            <select name="senhasup" id="senhasup" class="form-control col-sm-4"> 
                <option value="t"> Sim</option>
                <option value="f"> Não</option>
            </select>
        </div>
    </div> 
 </div>   
    <input  name="operacao" type="hidden"  id="operacao" value='novo'>
</div>
<div class="border-top">
<div class="card-body">
    <button type="submit" class="btn btn-primary">Gravar</button>
    <button type="button" class="btn btn-primary" data-dismiss="modal">Sair</button>
</div>
</div>
</form>
    </div>
  </div>
</div>
 </form>               
  <?php echo ($footer) ?>

        </div>

    </div>

    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <script src="../../dist/js/myjs.js"></script>
    <script src="../../dist/js/jquery.mask.js"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="../../assets/libs/flot/excanvas.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.pie.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.time.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.stack.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.crosshair.js"></script>
    <script src="../../assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="../../dist/js/pages/chart/chart-page-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/jquery.multicheck.js"></script>
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>

        <script>
        $('#zero_config').DataTable();
                $(document).ready(function(){
                $('#cpf').mask('000.000.000-00', {reverse: true});
                $('#cep').mask('00000-000');
                $('#telefone').mask('(00) 0000-0000');
                $('#celular').mask('(00) 00000-0000');
            });
    </script>
</body>

</html>