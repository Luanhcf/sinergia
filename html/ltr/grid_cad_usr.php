<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:authentication-login.php");
  exit;
}
require_once('src/menu.php');
require_once ('src/conexao.php');
$operacao = "";
$operacao = isset($_POST['operacao']) ? $_POST['operacao'] : '';
$alert = isset($_GET['alert']) ? $_GET['alert'] : '';
$msg="";
if ($alert==1){
$msg="<div style=\"text-align:center\" class=\"alert alert-success\" role=\"alert\">
  Cadastro realizado com sucesso!
</div>";
}

// VARIAVEIS DA PAGINA
$login    = "";
$senha   = "";
$nivel    = "";
$func     = "";

 if ($operacao == "novo"){

$login       = isset($_POST['login']) ? $_POST['login'] : '';
$senha    = isset($_POST['senha']) ? $_POST['senha'] : '';
$nivel   = isset($_POST['nivel']) ? $_POST['nivel'] : '';
$func    = isset($_POST['func']) ? $_POST['func'] : '';

$sql = "insert into usuarios (login,senha,nivel,funcionario) values  ('$login',md5('$senha'),'$nivel','$func')";


 $exec = pg_exec($conexao,$sql); 
  header ("location:grid_cad_usr.php?alert=1");

 }
if ($operacao == "editar"){
$id     = isset($_POST['id']) ? $_POST['id'] : '';

$login       = isset($_POST['login']) ? $_POST['login'] : '';
$senha    = isset($_POST['senha']) ? $_POST['senha'] : '';
$nivel   = isset($_POST['nivel']) ? $_POST['nivel'] : '';
$func    = isset($_POST['func']) ? $_POST['func'] : '';

if ($senha == ""){

  $sql = "update usuarios set login='$login',nivel='$nivel',funcionario='$func' where id=$id";  

} else {

    $sql = "update usuarios set login='$login',nivel='$nivel',funcionario='$func',senha=md5('$senha') where id=$id";
}

 $exec = pg_exec($conexao,$sql); 

}

if ($operacao == "carregar"){

$id  = isset($_POST['id']) ? $_POST['id'] : '';
$ret = "select * From usuarios where id=$id";
$sqlrg = pg_query($conexao,$ret);
$res = pg_fetch_assoc($sqlrg);


print(json_encode($res));
 
 exit;

}

// CARREGANDO GRID PRINCIPAL
$exibegrid = "";
$sqlg = "select u.id,
       login,
       f.apelido   AS funcionario,
       n.id||' - '||n.descricao AS nivel
from   usuarios u
       INNER JOIN niveis n
               ON ( u.nivel = n.id )
       INNER JOIN funcionarios f
               ON ( u.funcionario = f.id )";
$sqlrg = pg_query($conexao,$sqlg); 

while ($row=pg_fetch_assoc($sqlrg)){
   $exibegrid.= "<tr>
                <td><a href=\"#\" onclick=\"editarcaduser(".$row["id"].")\">".$row["id"]."</a></td>
                <td>".$row["login"]."</td>
                <td>".$row["funcionario"]."</td>
                <td>".$row["nivel"]."</td>
                </tr>";
}


// Carregar Niveis
$exibenivel = "";
$sqlnivel ="";
$sqlnivel = "select id,id||' - '||descricao as descricao from niveis order by id";
$sqlnivelresul = pg_query($conexao,$sqlnivel);
while ($row2=pg_fetch_assoc($sqlnivelresul)){

    $exibenivel.= "<option value=\"".$row2["id"]."\">".$row2["descricao"]." </option>";

    }

// Carregar Funcionarios
$exibefunc = "";
$sqlfunc ="";
$sqlfunc = "select id,id||' - '||apelido as apelido from funcionarios order by id";
$sqlfuncresul = pg_query($conexao,$sqlfunc);
while ($row2=pg_fetch_assoc($sqlfuncresul)){

    $exibefunc.= "<option value=\"".$row2["id"]."\">".$row2["apelido"]." </option>";

    }    

?>



<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <?php echo ($title); ?>
    <!-- Custom CSS -->
    <link href="../../assets/libs/flot/css/float-chart.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <style>
     .divide {
        display:grid;
        grid-row : 1;
        grid-template-columns: 50% 50%;

     }   
    </style>

</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">
        <!-- MENU LATERAL A BUSCA SUPERIOR -->
         <?php echo ($header);
               echo ($mlateral); 
         ?>

<div class="page-wrapper">
    <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Cadastro de Usuários</h5>
                        <div class="table-responsive">
                            <div style="text-align:center">
             <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target=".bd-example-modal-lg">Inserir</button>
                                </div>
                <?php echo($msg); ?>
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    
                                    <tr>
                                        <th></th>
                                        <th>Login</th>
                                        <th>Funcionário</th>
                                        <th>Nivel</th>
                                    </tr>

                                </thead>
                                <tbody>
                                <?php
                                echo ($exibegrid);
                                ?>   

                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>
 <!--DESENHANDO NOVO CLIENTE MODAL -->
 <form class="form-horizontal" method="POST" action="grid_cad_usr.php" target="_self">
 <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="abremodal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--CONTEUDO -->
<br>   
<h4 class="card-title">Cadastro de Usuários</h4>     
<div class="card-body">
    <div class="form-group row">
        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Codigo</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-2" name="id" id="id" readonly="true">
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Login</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-5" id="login" name="login"  required>
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Digite sua senha</label>
        <div class="col-sm-9">
            <input type="password" class="form-control col-sm-5" name="senha" id="senha" maxlength="100">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Nivel</label>
        <div class="col-sm-9">
            <select class="form-control col-sm-5" name="nivel" id="nivel"> 
             <?php
              echo $exibenivel;
             ?>   
            </select>    
        </div>
    </div>    
    <div class="form-group row">
        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Funcionário</label>
        <div class="col-sm-9">
            <select class="form-control col-sm-5" name="func" id="func"> 
             <?php
              echo $exibefunc;
             ?>   
            </select>    
        </div>
    </div>
    <input  name="operacao" type="hidden"  id="operacao" value='novo'>
</div>
<div class="border-top">
<div class="card-body">
    <button type="submit" onclick="validasenha();" class="btn btn-primary">Gravar</button>
    <button type="button" class="btn btn-primary" data-dismiss="modal">Sair</button>
</div>
</div>
</form>
    </div>
  </div>
</div>
 </form>               
  <?php echo ($footer) ?>

        </div>

    </div>

    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <script src="../../dist/js/myjs.js"></script>
    <script src="../../dist/js/jquery.mask.js"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="../../assets/libs/flot/excanvas.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.pie.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.time.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.stack.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.crosshair.js"></script>
    <script src="../../assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="../../dist/js/pages/chart/chart-page-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/jquery.multicheck.js"></script>
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>

        <script>
        $('#zero_config').DataTable();

    </script>
</body>

</html>