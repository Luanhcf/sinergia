<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:authentication-login.php");
  exit;
}
require_once('src/menu.php');
require_once ('src/conexao.php');

$sql = "select 
(select count(id)  from   movimentacao m where ((m.origem='S' and aciona_dev='t' and concluido='t' and iniciar='t') or (origem='D' and iniciar='t' and pause='f' and concluido='f' and aciona_dev='f'))) as fazendo,
(select count(id) from   movimentacao m where ((m.origem='S' and aciona_dev='t' and concluido='t') or (origem='D')) and (iniciar='f' and pause='f' and concluido='f' and aciona_dev='f')) as aberto,
(select count(id) from   movimentacao m where (m.origem='S' and aciona_dev='t' and concluido='t' and concluido_dev) or (concluido='t' and concluido_dev='t' and origem='D')) as concluido,
(select count(id) from movimentacao where ((origem='S' and aciona_dev='t' and concluido='t') or (origem='D')) ) as total
";

$sqlquery = pg_query($conexao,$sql);
$res = pg_fetch_assoc($sqlquery);
$fazendo = $res['fazendo'];
$aberto = $res['aberto'];
$concluido = $res['concluido'];
$total = $res['total'];

$exibegrid = "";

$sqlchama = "select m.id,
       c.fantasia as cliente,
       sis.descricao as sistema,
       m.descr_chamado as descr_chamado,
       to_char(m.dt_abertura,'dd-mm-yyyy') as dt_abertura,
       case when m.prioridade=0 then 'A Definir' else pri.descricao end as prioridade,
       case when m.desenv=0 then 'A Definir' else fun.apelido end as desenv,
       case when data_hora_fim_dev is null then to_char(current_timestamp - data_hora_inicio_dev,'HH24:MI:SS') else to_char(m.data_hora_fim_dev -  data_hora_inicio_dev,'HH24:MI:SS') end as tempogasto
from   movimentacao m
       left join clientes c
               on ( m.cliente = c.id )
       left join funcionarios f
               on ( m.tecnico = f.id )
       left join servicos_tec s
               on ( m.servico = s.id ) 
       left join tipo_atendimento t
           on ( m.tipo_atend = t.id )
       left join sistemas sis
           on ( m.sistema = sis.id )    
       left join tipo_entrada te
           on ( m.tipo_entrada = te.id )
       left join setores se
           on ( m.setor = se.id ) 
       left join modulos mod
           on   (m.modulo = mod.id)
       left join prioridades pri
           on   (m.prioridade = pri.id)
       left join sprint sp
           on (m.sprint = sp.id)
       left join funcionarios fun
             on ( m.desenv = fun.id )
WHERE  ((m.origem='S' AND aciona_dev='t' AND concluido='t' AND iniciar='t')
 OR (origem='D' AND iniciar='t' AND pause='f' AND concluido='f' AND aciona_dev='f')) ";

$sqlrg = pg_query($conexao,$sqlchama); 

while ($row=pg_fetch_assoc($sqlrg)){

$id = $row['id'];
$cliente = $row['cliente'];
$sistema = $row['sistema'];
$descr_chamado = $row['descr_chamado'];
$dt_abertura = $row['dt_abertura'];
$prioridade = $row['prioridade'];
$desenv = $row['desenv'];
$tempogasto = $row['tempogasto'];

$exibegrid.="
<tr>
<th scope=\"row\">$id</th>
<td>$cliente</td>
<td>$sistema</td>
<td>$descr_chamado</td>
<td>$dt_abertura</td>
<td>$prioridade</td>
<td>$desenv</td>
<td>$tempogasto</td>
</tr>";

}



?>



<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--<meta http-equiv="refresh" content="10;url=monitor_geral.php"> -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <?php echo ($title); ?>
    <!-- Custom CSS -->
    <link href="../../assets/libs/flot/css/float-chart.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <style>
     .divide {
        display:grid;
        grid-row : 1;
        grid-template-columns: 50% 50%;

     }   
    </style>

</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">
        <!-- MENU LATERAL A BUSCA SUPERIOR -->
         <?php echo ($header);
               echo ($mlateral); 
         ?>

<div class="page-wrapper">
    <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Monitoramento Geral</h4>
                        <h6 class="card-title">Desenvolvimento</h6>
<!-- ESCREVER AQUI O CODIGO-->

                <div class="row">
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <div class="box bg-cyan text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-view-dashboard"></i></h1>
                                <h6 class="text-white">Desenvolvimento</h6>
                                <h6 class="text-white"><?php echo $fazendo; ?></h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <div class="box bg-success text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-chart-areaspline"></i></h1>
                                <h6 class="text-white">Concluidos</h6>
                                <h6 class="text-white"><?php echo $concluido; ?></h6>
                            </div>
                        </div>
                    </div>
                     <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <div class="box bg-warning text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-collage"></i></h1>
                                <h6 class="text-white">Abertos</h6>
                                <h6 class="text-white"><?php echo $aberto; ?></h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <div class="box bg-info text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-arrow-all"></i></h1>
                                <h6 class="text-white">Total</h6>
                                <h6 class="text-white"><?php echo $total; ?></h6>
                            </div>
                        </div>
                    </div>

                </div>
                      

                    </div>
                </div>
                        
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Em Desenvolvimento</h5>
                    </div>
                    <table class="table table-sm">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Cliente</th>
                          <th scope="col">Sistema</th>
                          <th scope="col">Detalhe</th>
                          <th scope="col">Data Abertura</th>
                          <th scope="col">Prioridade</th>
                          <th scope="col">Programador</th>
                          <th scope="col">Tempo Gasto</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                            echo $exibegrid;
                        ?>
                         
                        
                      </tbody>
                    </table>
                </div>              
 
  <?php echo ($footer) ?>

        </div>

    </div>

    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <script src="../../dist/js/myjs.js"></script>
    <script src="../../dist/js/jquery.mask.js"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="../../assets/libs/flot/excanvas.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.pie.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.time.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.stack.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.crosshair.js"></script>
    <script src="../../assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="../../dist/js/pages/chart/chart-page-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/jquery.multicheck.js"></script>
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>

        <script>
        $('#zero_config').DataTable();

    </script>
</body>

</html>