<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:authentication-login.php");
  exit;
}
require_once('src/menu.php');
require_once ('src/conexao.php');
$operacao = "";
$operacao = isset($_POST['operacao']) ? $_POST['operacao'] : '';
$alert = isset($_GET['alert']) ? $_GET['alert'] : '';
$msg="";
if ($alert==1){
$msg="<div style=\"text-align:center\" class=\"alert alert-success\" role=\"alert\">
  Cadastro realizado com sucesso!
</div>";
}

// VARIAVEIS DA PAGINA
$cnpj     = "";
$razao    = "";
$fantasia = "";
$insc     = "";
$cep      = "";
$endereco = "";
$bairro   = "";
$cidade   = "";
$uf       = "";
$compl    = "";
$email    = "";
$cpf      = "";
$telefone = "";
$celular  = "";
$pessoa   = "";
$contato  = "";
$grupo    = "";
$obs      = "";



 if ($operacao == "novo"){

$cnpj     = isset($_POST['cnpj']) ? $_POST['cnpj'] : '';
$razao    = isset($_POST['razao']) ? $_POST['razao'] : '';
$fantasia = isset($_POST['fantasia']) ? $_POST['fantasia'] : '';
$insc     = isset($_POST['insc']) ? $_POST['insc'] : '';
$cep      = isset($_POST['cep']) ? $_POST['cep'] : '';
$endereco = isset($_POST['endereco']) ? $_POST['endereco'] : '';
$bairro   = isset($_POST['bairro']) ? $_POST['bairro'] : '';
$cidade   = isset($_POST['cidade']) ? $_POST['cidade'] : '';
$uf       = isset($_POST['uf']) ? $_POST['uf'] : '';
$compl    = isset($_POST['compl']) ? $_POST['compl'] : '';
$email    = isset($_POST['email']) ? $_POST['email'] : '';
$cpf      = isset($_POST['cpf']) ? $_POST['cpf'] : '';
$telefone = isset($_POST['telefone']) ? $_POST['telefone'] : '';
$celular  = isset($_POST['celular']) ? $_POST['celular'] : '';
$pessoa   = isset($_POST['pessoa']) ? $_POST['pessoa'] : '';
$contato  = isset($_POST['contato']) ? $_POST['contato'] : '';
$grupo    = isset($_POST['grupo']) ? $_POST['grupo'] : '';
$obs      = isset($_POST['obs']) ? $_POST['obs'] : '';



$sql = "insert into clientes (cnpj,razao,fantasia,insc,cep,endereco,bairro,cidade,uf,compl,email,cpf,telefone,celular,pessoa,contato,grupo,obs) values  (regexp_replace('$cnpj', '[^0-9]', '', 'gi'),'$razao','$fantasia','$insc',regexp_replace('$cep', '[^0-9]', '', 'gi'),'$endereco','$bairro','$cidade','$uf','$compl','$email',regexp_replace('$cpf', '[^0-9]', '', 'gi'),regexp_replace('$telefone', '[^0-9]', '', 'gi'),regexp_replace('$celular', '[^0-9]', '', 'gi'),'$pessoa','$contato','$grupo','$obs')";


 $exec = pg_exec($conexao,$sql); 
  header ("location:grid_cliente.php?alert=1");

 }
if ($operacao == "editar"){
$id     = isset($_POST['id']) ? $_POST['id'] : '';

$cnpj     = isset($_POST['cnpj']) ? $_POST['cnpj'] : '';
$razao    = isset($_POST['razao']) ? $_POST['razao'] : '';
$fantasia = isset($_POST['fantasia']) ? $_POST['fantasia'] : '';
$insc     = isset($_POST['insc']) ? $_POST['insc'] : '';
$cep      = isset($_POST['cep']) ? $_POST['cep'] : '';
$endereco = isset($_POST['endereco']) ? $_POST['endereco'] : '';
$bairro   = isset($_POST['bairro']) ? $_POST['bairro'] : '';
$cidade   = isset($_POST['cidade']) ? $_POST['cidade'] : '';
$uf       = isset($_POST['uf']) ? $_POST['uf'] : '';
$compl    = isset($_POST['compl']) ? $_POST['compl'] : '';
$email    = isset($_POST['email']) ? $_POST['email'] : '';
$cpf      = isset($_POST['cpf']) ? $_POST['cpf'] : '';
$telefone = isset($_POST['telefone']) ? $_POST['telefone'] : '';
$celular  = isset($_POST['celular']) ? $_POST['celular'] : '';
$pessoa   = isset($_POST['pessoa']) ? $_POST['pessoa'] : '';
$contato  = isset($_POST['contato']) ? $_POST['contato'] : '';
$grupo    = isset($_POST['grupo']) ? $_POST['grupo'] : '';
$obs      = isset($_POST['obs']) ? $_POST['obs'] : '';


$sql = "update clientes set cnpj=regexp_replace('$cnpj', '[^0-9]', '', 'gi'),razao='$razao',fantasia='$fantasia',insc='$insc',cep=regexp_replace('$cep', '[^0-9]', '', 'gi'),endereco='$endereco',bairro='$bairro',cidade='$cidade',uf='$uf',compl='$compl',email='$email',cpf=regexp_replace('$cpf', '[^0-9]', '', 'gi'),telefone=regexp_replace('$telefone', '[^0-9]', '', 'gi'),celular=regexp_replace('$celular', '[^0-9]', '', 'gi'),pessoa='$pessoa',contato='$contato',grupo='$grupo',obs='$obs' where id=$id";

 $exec = pg_exec($conexao,$sql); 



}

if ($operacao == "carregar"){

$id  = isset($_POST['id']) ? $_POST['id'] : '';
$ret = "select * From clientes where id=$id";
$sqlrg = pg_query($conexao,$ret);
$res = pg_fetch_assoc($sqlrg);


print(json_encode($res));
 
 exit;

}

// CARREGANDO GRID PRINCIPAL
$exibegrid = "";
$sqlg = "select id,razao,fantasia,cnpj,uf,cidade,telefone,celular from clientes";
$sqlrg = pg_query($conexao,$sqlg); 

while ($row=pg_fetch_assoc($sqlrg)){
   $exibegrid.= "<tr>
                <td><a href=\"#\" onclick=\"editarcadcli(".$row["id"].")\">".$row["id"]."</a></td>
                <td>".$row["razao"]."</td>
                <td>".$row["fantasia"]."</td>
                <td>".$row["cnpj"]."</td>
                <td>".$row["uf"]."</td>
                <td>".$row["cidade"]."</td>
                <td>".$row["telefone"]."</td>
                <td>".$row["celular"]."</td>
                </tr>";
}

//CARREGAR UFS DA PAGINA

$griduf ="";
$sqluf = "select distinct(uf) as uf from cep_cidades";
$sqlufresult = pg_query($conexao,$sqluf);
while ($row1=pg_fetch_assoc($sqlufresult)){

    $griduf.= "<option value=\"".$row1["uf"]."\">".$row1["uf"]." </option>";

    }
?>



<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <?php echo ($title); ?>
    <!-- Custom CSS -->
    <link href="../../assets/libs/flot/css/float-chart.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <style>
     .divide {
        display:grid;
        grid-row : 1;
        grid-template-columns: 50% 50%;

     }   
    </style>

</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">
        <!-- MENU LATERAL A BUSCA SUPERIOR -->
         <?php echo ($header);
               echo ($mlateral); 
         ?>

<div class="page-wrapper">
    <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Cadastro de Clientes</h5>
                        <div class="table-responsive">
                            <div style="text-align:center">
             <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target=".bd-example-modal-lg">Inserir</button>
                                </div>
                <?php echo($msg); ?>
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    
                                    <tr>
                                        <th></th>
                                        <th>Razão</th>
                                        <th>Fantasia</th>
                                        <th>CNPJ</th>
                                        <th>UF</th>
                                        <th>Cidade</th>
                                        <th>Telefone</th>
                                        <th>Celular</th>
                                    </tr>

                                </thead>
                                <tbody>
                                <?php
                                echo ($exibegrid);
                                ?>   

                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>
 <!--DESENHANDO NOVO CLIENTE MODAL -->
 <form class="form-horizontal" method="POST" action="grid_cliente.php" target="_self">
 <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="abremodal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--CONTEUDO -->
<br>   
<h4 class="card-title">Cadastro de Cliente</h4>     
<div class="card-body divide">
    <div class="form-group row">
        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Codigo</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-2" name="id" id="id" readonly="true">
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">CNPJ</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-7" id="cnpj" name="cnpj"  required>
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Razão social</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="razao" id="razao" maxlength="100">
        </div>
    </div>
    <div class="form-group row">
        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Fantasia</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="fantasia" id="fantasia" maxlength="100">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Inscrição Estadual</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-6" id="insc" name="insc" maxlength="20">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Cep</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-6" id="cep" name="cep" maxlength="8">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Endereço</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="endereco" name="endereco">
        </div>
    </div>  
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Bairro</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="bairro" name="bairro">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Cidade</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="cidade" name="cidade">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">UF</label>
        <div class="col-sm-9">
            <!--<input type="text" class="form-control col-sm-2" id="uf" name="uf"> -->
            <select class="custom-select col-sm-3" id="uf" name="uf" >
            <?php echo($griduf); ?>   
            </select>
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Complemento</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="compl" name="compl">
        </div>
    </div>   
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">E-mail</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="email" name="email">
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">CPF</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-6" id="cpf" name="cpf">
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Telefone</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-6" id="telefone" name="telefone">
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Celular</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-6" id="celular" name="celular">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Pessoa</label>
        <div class="col-sm-9">
            <select class="custom-select col-sm-6" id="pessoa" name="pessoa" >
            <option value="F">Fisica</option> 
            <option value="J">Juridica</option>   
            </select>
        </div>
    </div>  
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Contato</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="contato" name="contato">
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Grupo</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="grupo" name="grupo" value ="<?php echo ($grupo); ?>">
        </div>
    </div>                                                                      
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Observações</label>
        <div class="col-sm-9">
            <textarea class="form-control" name="obs" id="obs" value ="<?php echo ($obs); ?>"></textarea>
        </div>
    </div>
    <input  name="operacao" type="hidden"  id="operacao" value='novo'>
</div>
<div class="border-top">
<div class="card-body">
    <button type="submit" class="btn btn-primary">Gravar</button>
    <button type="button" class="btn btn-primary" data-dismiss="modal">Sair</button>
</div>
</div>
</form>
    </div>
  </div>
</div>
 </form>               
  <?php echo ($footer) ?>

        </div>

    </div>

    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <script src="../../dist/js/myjs.js"></script>
    <script src="../../dist/js/jquery.mask.js"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="../../assets/libs/flot/excanvas.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.pie.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.time.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.stack.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.crosshair.js"></script>
    <script src="../../assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="../../dist/js/pages/chart/chart-page-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/jquery.multicheck.js"></script>
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>

        <script>
        $('#zero_config').DataTable();
                $(document).ready(function(){
                $('#cnpj').mask('00.000.000/0000-00', {reverse: true});
                $('#cpf').mask('000.000.000-00', {reverse: true});
                $('#cep').mask('00000-000');
                $('#telefone').mask('(00) 0000-0000');
                $('#celular').mask('(00) 00000-0000');
            });
    </script>
</body>

</html>