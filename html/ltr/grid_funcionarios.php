<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:authentication-login.php");
  exit;
}
require_once('src/menu.php');
require_once ('src/conexao.php');
$operacao = "";
$operacao = isset($_POST['operacao']) ? $_POST['operacao'] : '';
$alert = isset($_GET['alert']) ? $_GET['alert'] : '';
$msg="";
if ($alert==1){
$msg="<div style=\"text-align:center\" class=\"alert alert-success\" role=\"alert\">
  Cadastro realizado com sucesso!
</div>";
}

// VARIAVEIS DA PAGINA
$nome     = "";
$apelido  = "";
$telefone = "";
$celular  = "";
$setor    = "";
$funcao   = "";
$cep      = "";
$endereco = "";
$bairro   = "";
$cidade   = "";
$uf       = "";
$status   = "";
$email    = "";
$cpf      = "";
$obs      = "";
$sup      = "";
$dev      = "";



 if ($operacao == "novo"){

$nome       = isset($_POST['nome']) ? $_POST['nome'] : '';
$apelido    = isset($_POST['apelido']) ? $_POST['apelido'] : '';
$telefone   = isset($_POST['telefone']) ? $_POST['telefone'] : '';
$celular    = isset($_POST['celular']) ? $_POST['celular'] : '';
$setor      = isset($_POST['setor']) ? $_POST['setor'] : '';
$funcao     = isset($_POST['funcao']) ? $_POST['funcao'] : '';
$cep        = isset($_POST['cep']) ? $_POST['cep'] : '';
$endereco   = isset($_POST['endereco']) ? $_POST['endereco'] : '';
$bairro     = isset($_POST['bairro']) ? $_POST['bairro'] : '';
$cidade     = isset($_POST['cidade']) ? $_POST['cidade'] : '';
$uf         = isset($_POST['uf']) ? $_POST['uf'] : '';
$status     = isset($_POST['status']) ? $_POST['status'] : '';
$email      = isset($_POST['email']) ? $_POST['email'] : '';
$cpf        = isset($_POST['cpf']) ? $_POST['cpf'] : '';
$obs        = isset($_POST['obs']) ? $_POST['obs'] : '';
$sup        = isset($_POST['sup']) ? $_POST['sup'] : '';
$dev        = isset($_POST['dev']) ? $_POST['dev'] : '';



$sql = "insert into funcionarios (nome,apelido,telefone,celular,setor,funcao,cep,endereco,bairro,cidade,uf,status,email,cpf,obs,data_cadastro,tecnico,dev) values  ('$nome','$apelido',regexp_replace('$telefone', '[^0-9]', '', 'gi'),regexp_replace('$celular', '[^0-9]', '', 'gi'),'$setor','$funcao',regexp_replace('$cep', '[^0-9]', '', 'gi'),'$endereco','$bairro','$cidade','$uf','$status','$email',regexp_replace('$cpf', '[^0-9]', '', 'gi'),'$obs',current_date,'$sup','$dev')";


 $exec = pg_exec($conexao,$sql); 
  header ("location:grid_funcionarios.php?alert=1");

 }
if ($operacao == "editar"){
$id     = isset($_POST['id']) ? $_POST['id'] : '';

$nome     = isset($_POST['nome']) ? $_POST['nome'] : '';
$apelido    = isset($_POST['apelido']) ? $_POST['apelido'] : '';
$telefone = isset($_POST['telefone']) ? $_POST['telefone'] : '';
$celular  = isset($_POST['celular']) ? $_POST['celular'] : '';
$setor = isset($_POST['setor']) ? $_POST['setor'] : '';
$funcao     = isset($_POST['funcao']) ? $_POST['funcao'] : '';
$cep      = isset($_POST['cep']) ? $_POST['cep'] : '';
$endereco = isset($_POST['endereco']) ? $_POST['endereco'] : '';
$bairro   = isset($_POST['bairro']) ? $_POST['bairro'] : '';
$cidade   = isset($_POST['cidade']) ? $_POST['cidade'] : '';
$uf       = isset($_POST['uf']) ? $_POST['uf'] : '';
$status    = isset($_POST['status']) ? $_POST['status'] : '';
$email    = isset($_POST['email']) ? $_POST['email'] : '';
$cpf      = isset($_POST['cpf']) ? $_POST['cpf'] : '';
$obs      = isset($_POST['obs']) ? $_POST['obs'] : '';
$sup        = isset($_POST['sup']) ? $_POST['sup'] : '';
$dev        = isset($_POST['dev']) ? $_POST['dev'] : '';

$sql = "update funcionarios set nome='$nome',apelido='$apelido',telefone=regexp_replace('$telefone', '[^0-9]', '', 'gi'),celular=regexp_replace('$celular', '[^0-9]', '', 'gi'),setor='$setor',funcao='$funcao',cep=regexp_replace('$cep', '[^0-9]', '', 'gi'),endereco='$endereco',bairro='$bairro',cidade='$cidade',uf='$uf',status='$status',email='$email',cpf=regexp_replace('$cpf', '[^0-9]', '', 'gi'),obs='$obs',tecnico='$sup',dev='$dev' where id=$id";

 $exec = pg_exec($conexao,$sql); 
//var_dump($sql);
//exit;


}

if ($operacao == "carregar"){

$id  = isset($_POST['id']) ? $_POST['id'] : '';
$ret = "select * From funcionarios where id=$id";
$sqlrg = pg_query($conexao,$ret);
$res = pg_fetch_assoc($sqlrg);


print(json_encode($res));
 
 exit;

}

// CARREGANDO GRID PRINCIPAL
$exibegrid = "";
$sqlg = "select * from funcionarios";
$sqlrg = pg_query($conexao,$sqlg); 

while ($row=pg_fetch_assoc($sqlrg)){
   $exibegrid.= "<tr>
                <td><a href=\"#\" onclick=\"editarcadfunc(".$row["id"].")\">".$row["id"]."</a></td>
                <td>".$row["apelido"]."</td>
                <td>".$row["setor"]."</td>
                <td>".$row["funcao"]."</td>
                <td>".$row["telefone"]."</td>
                <td>".$row["celular"]."</td>
                </tr>";
}

//CARREGAR UFS DA PAGINA

$griduf ="";
$sqluf = "select distinct(uf) as uf from cep_cidades";
$sqlufresult = pg_query($conexao,$sqluf);
while ($row1=pg_fetch_assoc($sqlufresult)){

    $griduf.= "<option value=\"".$row1["uf"]."\">".$row1["uf"]." </option>";

    }
?>



<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <?php echo ($title); ?>
    <!-- Custom CSS -->
    <link href="../../assets/libs/flot/css/float-chart.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <style>
     .divide {
        display:grid;
        grid-row : 1;
        grid-template-columns: 50% 50%;

     }   
    </style>

</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">
        <!-- MENU LATERAL A BUSCA SUPERIOR -->
         <?php echo ($header);
               echo ($mlateral); 
         ?>

<div class="page-wrapper">
    <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Cadastro de Funcionários</h5>
                        <div class="table-responsive">
                            <div style="text-align:center">
             <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target=".bd-example-modal-lg">Inserir</button>
                                </div>
                <?php echo($msg); ?>
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    
                                    <tr>
                                        <th></th>
                                        <th>Nome</th>
                                        <th>Setor</th>
                                        <th>Função</th>
                                        <th>Telefone</th>
                                        <th>Celular</th>
                                    </tr>

                                </thead>
                                <tbody>
                                <?php
                                echo ($exibegrid);
                                ?>   

                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>
 <!--DESENHANDO NOVO CLIENTE MODAL -->
 <form class="form-horizontal" method="POST" action="grid_funcionarios.php" target="_self">
 <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="abremodal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--CONTEUDO -->
<br>   
<h4 class="card-title">Cadastro de Funcionários</h4>     
<div class="card-body divide">
    <div class="form-group row">
        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Codigo</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-2" name="id" id="id" readonly="true">
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Nome</label>
        <div class="col-sm-9">
            <input type="text" class="form-control " id="nome" name="nome"  required>
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Apelido</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="apelido" id="apelido" maxlength="100">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Telefone</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-6" id="telefone" name="telefone">
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Celular</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-6" id="celular" name="celular">
        </div>
    </div>    
    <div class="form-group row">
        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Setor</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="setor" id="setor" maxlength="100">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Função</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-6" id="funcao" name="funcao" maxlength="20">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Cep</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-6" id="cep" name="cep" maxlength="8">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Endereço</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="endereco" name="endereco">
        </div>
    </div>  
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Bairro</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="bairro" name="bairro">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Cidade</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="cidade" name="cidade">
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">UF</label>
        <div class="col-sm-9">
            <!--<input type="text" class="form-control col-sm-2" id="uf" name="uf"> -->
            <select class="custom-select col-sm-3" id="uf" name="uf" >
            <?php echo($griduf); ?>   
            </select>
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Status</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="status" name="status">
        </div>
    </div>   
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">E-mail</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="email" name="email">
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">CPF</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-6" id="cpf" name="cpf">
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Suporte</label>
        <div class="col-sm-9">
            <select class="custom-select col-sm-4" id="sup" name="sup" >
            <option value="t">Sim</option>
            <option selected value="f">Não</option>
        </select>
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Desenvol</label>
        <div class="col-sm-9">
            <select class="custom-select col-sm-4" id="dev" name="dev" >
            <option value="t">Sim</option>
            <option selected value="f">Não</option>
            </select>
        </div>
    </div>         
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Observações</label>
        <div class="col-sm-9">
            <textarea class="form-control" name="obs" id="obs"></textarea>
        </div>
    </div>
    <input  name="operacao" type="hidden"  id="operacao" value='novo'>
</div>
<div class="border-top">
<div class="card-body">
    <button type="submit" class="btn btn-primary">Gravar</button>
    <button type="button" class="btn btn-primary" data-dismiss="modal">Sair</button>
</div>
</div>
</form>
    </div>
  </div>
</div>
 </form>               
  <?php echo ($footer) ?>

        </div>

    </div>

    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <script src="../../dist/js/myjs.js"></script>
    <script src="../../dist/js/jquery.mask.js"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="../../assets/libs/flot/excanvas.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.pie.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.time.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.stack.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.crosshair.js"></script>
    <script src="../../assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="../../dist/js/pages/chart/chart-page-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/jquery.multicheck.js"></script>
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>

        <script>
        $('#zero_config').DataTable();
                $(document).ready(function(){
                $('#cpf').mask('000.000.000-00', {reverse: true});
                $('#cep').mask('00000-000');
                $('#telefone').mask('(00) 0000-0000');
                $('#celular').mask('(00) 00000-0000');
            });
    </script>
</body>

</html>