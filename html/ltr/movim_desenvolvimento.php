<?php
session_start() ;
if ($_SESSION ["login"] == ''){
  header ("location:authentication-login.php");
  exit;
}
require_once('src/menu.php');
require_once ('src/conexao.php');
$usr = $_SESSION ["login"];
$operacao = "";
$operacao = isset($_POST['operacao']) ? $_POST['operacao'] : '';
$operacao1 = isset($_POST['operacao1']) ? $_POST['operacao1'] : '';

//SQL PARA FILTROS DE CHAMADOS
$filtro = isset($_GET['filtro']) ? $_GET['filtro'] : '';
$sqlusr ="select funcionario from usuarios where login='$usr'";
$sqlqueryusr = pg_query($conexao,$sqlusr);
$userfil = pg_fetch_assoc($sqlqueryusr);
$userfil2 = $userfil['funcionario'];

$alert = isset($_GET['alert']) ? $_GET['alert'] : '';
$msg="";
if ($alert==1){
$msg="<div style=\"text-align:center\" class=\"alert alert-success\" role=\"alert\">
  Atendimento aberto com sucesso!
</div>";
}

// VARIAVEIS DA PAGINA
$data      = "";
$cliente   = "";
$tecnico   = "";
$servico   = "";
$tipoatend = "";
$sistema   = "";
$contato   = "";
$tpentrada = "";
$setor     = "";
$descricao = "";




 if ($operacao == "novo"){

$data       = isset($_POST['data']) ? $_POST['data'] : '';
$cliente    = isset($_POST['cliente']) ? $_POST['cliente'] : '';
$desenv     = isset($_POST['desenv']) ? $_POST['desenv'] : '';
$modulo     = isset($_POST['modulo']) ? $_POST['modulo'] : '';
$prioridade = isset($_POST['prioridade']) ? $_POST['prioridade'] : '';
$sistema    = isset($_POST['sistema']) ? $_POST['sistema'] : '';
$sprint     = isset($_POST['sprint']) ? $_POST['sprint'] : '';
$setor      = isset($_POST['setor']) ? $_POST['setor'] : '';
$descricao  = isset($_POST['descricao']) ? $_POST['descricao'] : '';

if ($desenv == ''){
   $desenv = 0;
}


$sql ="insert into movimentacao (cliente,desenv,modulo,prioridade,sistema,descr_chamado,sprint,setor,dt_abertura,origem)
    values ('$cliente','$desenv','$modulo','$prioridade','$sistema','$descricao','$sprint','$setor','$data','D')";
//var_dump($sql);
//exit;
 $exec = pg_exec($conexao,$sql); 
  header ("location:movim_desenvolvimento.php?filtro=geral");

 }


if ($operacao == "editar"){
$id     = isset($_POST['id']) ? $_POST['id'] : '';

$data       = isset($_POST['data']) ? $_POST['data'] : '';
$cliente    = isset($_POST['cliente']) ? $_POST['cliente'] : '';
$desenv     = isset($_POST['desenv']) ? $_POST['desenv'] : '';
$modulo     = isset($_POST['modulo']) ? $_POST['modulo'] : '';
$prioridade = isset($_POST['prioridade']) ? $_POST['prioridade'] : '';
$sistema    = isset($_POST['sistema']) ? $_POST['sistema'] : '';
$sprint     = isset($_POST['sprint']) ? $_POST['sprint'] : '';
$setor      = isset($_POST['setor']) ? $_POST['setor'] : '';
$descricao  = isset($_POST['descricao']) ? $_POST['descricao'] : '';


$sql = "update movimentacao set cliente='$cliente',desenv='$desenv',modulo='$modulo',prioridade='$prioridade',sistema='$sistema',descr_chamado='$descricao',sprint='$sprint',setor='$setor',dt_abertura='$data' where id=$id";

 $exec = pg_exec($conexao,$sql); 

}

if ($operacao1 == "concluir"){
$id     = isset($_POST['id']) ? $_POST['id'] : '';
$descr_dev = isset($_POST['descr_dev']) ? $_POST['descr_dev'] : '';

$sql1 ="select funcionario from usuarios where login='$usr'";
$sqlquery1 = pg_query($conexao,$sql1);
$userdev = pg_fetch_assoc($sqlquery1);
$userdev2 = $userdev['funcionario'];
$sql = "update movimentacao set descr_dev='$descr_dev',iniciar='f',concluido_dev='t',concluido='t',data_hora_fim_dev=current_timestamp,dev_atend=$userdev2 where id=$id";

 $exec = pg_exec($conexao,$sql); 
  header ("location:movim_desenvolvimento.php?filtro=geral");
}

if ($operacao1 == "acionardev"){
$id     = isset($_POST['id']) ? $_POST['id'] : '';
$detalhetec = isset($_POST['detalhetec']) ? $_POST['detalhetec'] : '';
$sql = "update movimentacao set iniciar='f',descr_tec='$detalhetec',concluido='t',aciona_dev='t',pause='f',data_hora_fim=current_timestamp where id=$id";
//var_dump($sql);
//exit;
$exec = pg_exec($conexao,$sql); 
header ("location:movim_desenvolvimento.php?filtro=geral");

}

if ($operacao == "carregar"){

$id  = isset($_POST['id']) ? $_POST['id'] : '';

$sqltempo = "select data_hora_inicio from movimentacao where id = $id";
$sqltempoquery = pg_query($conexao,$sqltempo);
$tempoini = pg_fetch_assoc($sqltempoquery);


$ret = "select * From movimentacao where id = $id order by id desc";
$sqlrg = pg_query($conexao,$ret);
$res = pg_fetch_assoc($sqlrg);


print(json_encode($res));
 
 exit;

}

if ($operacao == "atualizainicial"){

$id     = isset($_POST['id']) ? $_POST['id'] : '';
$idinsert     = isset($_POST['idinsert']) ? $_POST['idinsert'] : '';
$tecnico    = isset($_POST['tecnico']) ? $_POST['tecnico'] : '';
$sql = "update movimentacao set iniciar='t',data_hora_inicio_dev=current_timestamp where id=$id";

 $exec = pg_exec($conexao,$sql); 

}


// CARREGANDO GRID PRINCIPAL
$exibegrid = "";
if ($filtro == 'geral'){
 $sqlg = "select m.id,
       c.fantasia as cliente,
       case when m.tecnico=0 then 'A Definir' else f.apelido end as tecnico,
       s.descricao as servico,
       t.descricao as tipo_atend,
       sis.descricao as sistema,
       m.descr_chamado as descr_chamado,
       m.contato as contato,
       te.descricao as tipo_entrada,
       se.descricao as setor,
       to_char(m.dt_abertura,'dd-mm-yyyy') as dt_abertura,
       m.data_hora_inicio,
       m.data_hora_fim,
       case when (iniciar='f' and pause='f' and concluido='t' and aciona_dev='t' and concluido_dev='t') then 'Desenvolvida'
            when (iniciar='t' and pause='t' and concluido='f' and aciona_dev='f') then 'Chamado Pausado'
            when (iniciar='t' and pause='f' and concluido='t' and aciona_dev='t') then 'Desenvolvendo'
            when (iniciar='f' and pause='f' and concluido='t' and aciona_dev='f') then 'Concluido'
            when (iniciar='f' and pause='f' and concluido='t' and aciona_dev='t') then 'Aberto Suporte'
            when (iniciar='f' and pause='f' and concluido='f' and aciona_dev='f') then 'Aberto'  
            when (iniciar='t' and pause='f' and concluido='f' and aciona_dev='f') then 'Desenvolvendo'  else '' end as status,
       m.concluido as concluido,
       m.aciona_dev as aciona_dev,
       m.pause as pause,
       m.iniciar,
       case when data_hora_fim_dev is null then to_char(current_timestamp - data_hora_inicio_dev,'HH24:MI:SS') else to_char(m.data_hora_fim_dev -  data_hora_inicio_dev,'HH24:MI:SS') end as tempogasto,
       m.concluido_dev,
       m.sprint,
       case when m.modulo=0 then 'A Definir' else mod.descricao end as modulo,
       case when m.prioridade=0 then 'A Definir' else pri.descricao end as prioridade,
       sp.descricao as sprint,
       case when m.desenv=0 then 'A Definir' else fun.apelido end as desenv

from   movimentacao m
       left join clientes c
               on ( m.cliente = c.id )
       left join funcionarios f
               on ( m.tecnico = f.id )
       left join servicos_tec s
               on ( m.servico = s.id ) 
       left join tipo_atendimento t
           on ( m.tipo_atend = t.id )
       left join sistemas sis
           on ( m.sistema = sis.id )    
       left join tipo_entrada te
           on ( m.tipo_entrada = te.id )
       left join setores se
           on ( m.setor = se.id ) 
       left join modulos mod
           on   (m.modulo = mod.id)
       left join prioridades pri
           on   (m.prioridade = pri.id)
       left join sprint sp
           on (m.sprint = sp.id)
       left join funcionarios fun
             on ( m.desenv = fun.id )
       where ((m.origem='S' and aciona_dev='t' and concluido='t') or (origem='D'))  order by m.id desc";
} else if ($filtro == 'abertos'){
$sqlg = "select m.id,
       c.fantasia as cliente,
       case when m.tecnico=0 then 'A Definir' else f.apelido end as tecnico,
       s.descricao as servico,
       t.descricao as tipo_atend,
       sis.descricao as sistema,
       m.descr_chamado as descr_chamado,
       m.contato as contato,
       te.descricao as tipo_entrada,
       se.descricao as setor,
       to_char(m.dt_abertura,'dd-mm-yyyy') as dt_abertura,
       m.data_hora_inicio,
       m.data_hora_fim,
       case when (iniciar='f' and pause='f' and concluido='t' and aciona_dev='t' and concluido_dev='t') then 'Desenvolvida'
            when (iniciar='t' and pause='t' and concluido='f' and aciona_dev='f') then 'Chamado Pausado'
            when (iniciar='t' and pause='f' and concluido='t' and aciona_dev='t') then 'Desenvolvendo'
            when (iniciar='f' and pause='f' and concluido='t' and aciona_dev='f') then 'Concluido'
            when (iniciar='f' and pause='f' and concluido='t' and aciona_dev='t') then 'Aberto Suporte'
            when (iniciar='f' and pause='f' and concluido='f' and aciona_dev='f') then 'Aberto'  
            when (iniciar='t' and pause='f' and concluido='f' and aciona_dev='f') then 'Desenvolvendo'  else '' end as status,
       m.concluido as concluido,
       m.aciona_dev as aciona_dev,
       m.pause as pause,
       m.iniciar,
       case when data_hora_fim_dev is null then to_char(current_timestamp - data_hora_inicio_dev,'HH24:MI:SS') else to_char(m.data_hora_fim_dev -  data_hora_inicio_dev,'HH24:MI:SS') end as tempogasto,
       m.concluido_dev,
       m.sprint,
       case when m.modulo=0 then 'A Definir' else mod.descricao end as modulo,
       case when m.prioridade=0 then 'A Definir' else pri.descricao end as prioridade,
       sp.descricao as sprint,
       case when m.desenv=0 then 'A Definir' else fun.apelido end as desenv

from   movimentacao m
       left join clientes c
               on ( m.cliente = c.id )
       left join funcionarios f
               on ( m.tecnico = f.id )
       left join servicos_tec s
               on ( m.servico = s.id ) 
       left join tipo_atendimento t
           on ( m.tipo_atend = t.id )
       left join sistemas sis
           on ( m.sistema = sis.id )    
       left join tipo_entrada te
           on ( m.tipo_entrada = te.id )
       left join setores se
           on ( m.setor = se.id ) 
       left join modulos mod
           on   (m.modulo = mod.id)
       left join prioridades pri
           on   (m.prioridade = pri.id)
       left join sprint sp
           on (m.sprint = sp.id)
       left join funcionarios fun
             on ( m.desenv = fun.id )
       where ((m.origem='S' and aciona_dev='t' and concluido='t') or (origem='D')) and m.desenv=$userfil2 and (iniciar='f' and pause='f' and concluido='f' and aciona_dev='f') order by m.id desc";

}else if ($filtro == 'desenvolvendo'){
$sqlg = "select m.id,
       c.fantasia as cliente,
       case when m.tecnico=0 then 'A Definir' else f.apelido end as tecnico,
       s.descricao as servico,
       t.descricao as tipo_atend,
       sis.descricao as sistema,
       m.descr_chamado as descr_chamado,
       m.contato as contato,
       te.descricao as tipo_entrada,
       se.descricao as setor,
       to_char(m.dt_abertura,'dd-mm-yyyy') as dt_abertura,
       m.data_hora_inicio,
       m.data_hora_fim,
       case when (iniciar='f' and pause='f' and concluido='t' and aciona_dev='t' and concluido_dev='t') then 'Desenvolvida'
            when (iniciar='t' and pause='f' and concluido='t' and aciona_dev='t') then 'Desenvolvendo'
            when (iniciar='t' and pause='t' and concluido='f' and aciona_dev='f') then 'Chamado Pausado'
            when (iniciar='f' and pause='f' and concluido='t' and aciona_dev='f') then 'Concluido'
            when (iniciar='f' and pause='f' and concluido='t' and aciona_dev='t') then 'Aberto Suporte'
            when (iniciar='f' and pause='f' and concluido='f' and aciona_dev='f') then 'Aberto'  
            when (iniciar='t' and pause='f' and concluido='f' and aciona_dev='f') then 'Desenvolvendo'  else '' end as status,
       m.concluido as concluido,
       m.aciona_dev as aciona_dev,
       m.pause as pause,
       m.iniciar,
       case when data_hora_fim_dev is null then to_char(current_timestamp - data_hora_inicio_dev,'HH24:MI:SS') else to_char(m.data_hora_fim_dev -  data_hora_inicio_dev,'HH24:MI:SS') end as tempogasto,
       m.concluido_dev,
       m.sprint,
       case when m.modulo=0 then 'A Definir' else mod.descricao end as modulo,
       case when m.prioridade=0 then 'A Definir' else pri.descricao end as prioridade,
       sp.descricao as sprint,
       case when m.desenv=0 then 'A Definir' else fun.apelido end as desenv

from   movimentacao m
       left join clientes c
               on ( m.cliente = c.id )
       left join funcionarios f
               on ( m.tecnico = f.id )
       left join servicos_tec s
               on ( m.servico = s.id ) 
       left join tipo_atendimento t
           on ( m.tipo_atend = t.id )
       left join sistemas sis
           on ( m.sistema = sis.id )    
       left join tipo_entrada te
           on ( m.tipo_entrada = te.id )
       left join setores se
           on ( m.setor = se.id ) 
       left join modulos mod
           on   (m.modulo = mod.id)
       left join prioridades pri
           on   (m.prioridade = pri.id)
       left join sprint sp
           on (m.sprint = sp.id)
       left join funcionarios fun
             on ( m.desenv = fun.id )
       where ((m.origem='S' and aciona_dev='t' and concluido='t' and iniciar='t') or (origem='D' and iniciar='t' and pause='f' and concluido='f' and aciona_dev='f')) and m.desenv=$userfil2  order by m.id desc";

}else if ($filtro == 'concluido'){
$sqlg = "select m.id,
       c.fantasia as cliente,
       case when m.tecnico=0 then 'A Definir' else f.apelido end as tecnico,
       s.descricao as servico,
       t.descricao as tipo_atend,
       sis.descricao as sistema,
       m.descr_chamado as descr_chamado,
       m.contato as contato,
       te.descricao as tipo_entrada,
       se.descricao as setor,
       to_char(m.dt_abertura,'dd-mm-yyyy') as dt_abertura,
       m.data_hora_inicio,
       m.data_hora_fim,
       case when (iniciar='f' and pause='f' and concluido='t' and aciona_dev='t' and concluido_dev='t') then 'Concluido'
            when (iniciar='t' and pause='t' and concluido='f' and aciona_dev='f') then 'Chamado Pausado'
            when (iniciar='t' and pause='f' and concluido='t' and aciona_dev='t') then 'Desenvolvendo'
            when (iniciar='f' and pause='f' and concluido='t' and aciona_dev='f') then 'Concluido'
            when (iniciar='f' and pause='f' and concluido='t' and aciona_dev='t') then 'Aberto Suporte'
            when (iniciar='f' and pause='f' and concluido='f' and aciona_dev='f') then 'Aberto'  
            when (iniciar='t' and pause='f' and concluido='f' and aciona_dev='f') then 'Desenvolvendo'  else '' end as status,
       m.concluido as concluido,
       m.aciona_dev as aciona_dev,
       m.pause as pause,
       m.iniciar,
       case when data_hora_fim_dev is null then to_char(current_timestamp - data_hora_inicio_dev,'HH24:MI:SS') else to_char(m.data_hora_fim_dev -  data_hora_inicio_dev,'HH24:MI:SS') end as tempogasto,
       m.concluido_dev,
       m.sprint,
       case when m.modulo=0 then 'A Definir' else mod.descricao end as modulo,
       case when m.prioridade=0 then 'A Definir' else pri.descricao end as prioridade,
       sp.descricao as sprint,
       case when m.desenv=0 then 'A Definir' else fun.apelido end as desenv

from   movimentacao m
       left join clientes c
               on ( m.cliente = c.id )
       left join funcionarios f
               on ( m.tecnico = f.id )
       left join servicos_tec s
               on ( m.servico = s.id ) 
       left join tipo_atendimento t
           on ( m.tipo_atend = t.id )
       left join sistemas sis
           on ( m.sistema = sis.id )    
       left join tipo_entrada te
           on ( m.tipo_entrada = te.id )
       left join setores se
           on ( m.setor = se.id ) 
       left join modulos mod
           on   (m.modulo = mod.id)
       left join prioridades pri
           on   (m.prioridade = pri.id)
       left join sprint sp
           on (m.sprint = sp.id)
       left join funcionarios fun
             on ( m.desenv = fun.id )
       where ((iniciar='f' and pause='f' and concluido='t' and aciona_dev='t' and concluido_dev='t' and origem='S') or (iniciar='f' and pause='f' and concluido='t' and aciona_dev='f' and origem='D' )) and m.desenv=$userfil2 order by m.id desc";

}


$sqlrg = pg_query($conexao,$sqlg); 

while ($row=pg_fetch_assoc($sqlrg)){
    $id = $row["id"];
    $concluido = $row["concluido_dev"];
    $editar = $row["concluido_dev"] == "f" ? "Editar" : " ";
    $atender = $row["concluido_dev"] == "f" ? "Atender" : " ";
    $testainicio = $row["iniciar"] == "f"? "statusinicial($id)" : "";
    $cores_status = ($row["status"] == 'Concluido' ? 'chamconcluido':($row["status"] == 'Em Atendimento' ? 'chamatend' : ($row["status"] == 'Desenvolvida' ? 'chamconcluido' : ($row["status"] == 'Desenvolvendo' ? 'chamatend' : ($row["status"] == 'Aberto' ? 'chamaguardando' : ($row["status"] == 'Aberto Suporte' ? 'chamaguardando' : ''  ))))));

   $exibegrid.= "<tr>
                <td>
                <a href=\"#\" onclick=\"editarcadchamsup($id)\">$editar</a> 
                <a href=\"#\" onclick=\"verchamadosup($id)\">Detalhar</a> 
                <a ".($row["concluido_dev"] == "f" ? "onclick=\"if(!confirm('Deseja realmente iniciar o atendimento?')) return false;atendechamadosup($id);$testainicio\"":"")." href=\"#\">$atender</a> 
                </td>
                <td>".$row["id"]."</td>
                <td>".$row["cliente"]."</td>
                <td>".$row["desenv"]."</td>
                <td>".$row["sistema"]."</td>
                <td class=\"$cores_status\">".$row["status"]."</td>
                <td>".$row["modulo"]."</td>
                <td>".$row["prioridade"]."</td>
                <td>".$row["dt_abertura"]."</td>
                <td>".$row["tempogasto"]."</td>
                </tr>";
}

// Carregar clientes
$exibecli = "";
$sqlcli ="";
$sqlcli = "select id,id||'-'||fantasia||' ( '||substring(cnpj,1,2)||'.'||substring(cnpj,3,3)||'.'||substring(cnpj,6,3)||'/'||substring(cnpj,9,4)||'-'||substring(cnpj,13,2)||' ) ' as fantasia,fantasia as fantasia1 from clientes order by fantasia1 asc";
$sqlcliresul = pg_query($conexao,$sqlcli);
while ($row2=pg_fetch_assoc($sqlcliresul)){

    $exibecli.= "<option value=\"".$row2["id"]."\">".$row2["fantasia"]." </option>";

    }

//Carregar desenvolvedor
$exibetec = "";
$sqlfunc ="";
$sqlfunc = "select id,id||'-'||apelido as apelido from funcionarios where dev='t' order by apelido";
$sqlfuncresul = pg_query($conexao,$sqlfunc);
while ($row3=pg_fetch_assoc($sqlfuncresul)){

    $exibetec.= "<option value=\"".$row3["id"]."\">".$row3["apelido"]." </option>";

    }

//Carregar suporte
$exibesup = "";
$sqlsup ="";
$sqlsup = "select id,id||'-'||apelido as apelido from funcionarios where tecnico='t' order by apelido";
$sqlsupresul = pg_query($conexao,$sqlsup);
while ($row11=pg_fetch_assoc($sqlsupresul)){

    $exibesup.= "<option value=\"".$row11["id"]."\">".$row11["apelido"]." </option>";

    }    

//Carregar Serviço
$exibemod = "";
$sqlmod ="";
$sqlmod = "select id,id||'-'||descricao as descricao from modulos order by descricao";
$sqlmodresul = pg_query($conexao,$sqlmod);
while ($row4=pg_fetch_assoc($sqlmodresul)){

    $exibemod.= "<option value=\"".$row4["id"]."\">".$row4["descricao"]." </option>";

    }   

//Carregar Tipo de atendimento
$exibepriori = "";
$sqlpriori ="";
$sqlpriori = "select id,id||'-'||descricao as descricao from prioridades order by id";
$sqlprioriresul = pg_query($conexao,$sqlpriori);
while ($row5=pg_fetch_assoc($sqlprioriresul)){

    $exibepriori.= "<option value=\"".$row5["id"]."\">".$row5["descricao"]." </option>";

    }   

//Carregar Sistema
$exibesistema = "";
$sqlsist ="";
$sqlsist = "select id,id||'-'||descricao as descricao from sistemas order by descricao";
$sqlsistresul = pg_query($conexao,$sqlsist);
while ($row5=pg_fetch_assoc($sqlsistresul)){

    $exibesistema.= "<option value=\"".$row5["id"]."\">".$row5["descricao"]." </option>";

    }  

//Carregar Tipo de entrada
$exibesprint = "";
$sqlsprint ="";
$sqlsprint = "select id,id||'-'||descricao||' ('||data_inicial||' a '||data_final||')' as descricao from sprint order by id";
$sqlsprintresul = pg_query($conexao,$sqlsprint);
while ($row6=pg_fetch_assoc($sqlsprintresul)){

    $exibesprint.= "<option value=\"".$row6["id"]."\">".$row6["descricao"]." </option>";

    }   

//Carregar Setor
$exibesetor = "";
$sqlsetor ="";
$sqlsetor = "select id,id||'-'||descricao as descricao from setores order by descricao";
$sqlsetorresul = pg_query($conexao,$sqlsetor);
while ($row7=pg_fetch_assoc($sqlsetorresul)){

    $exibesetor.= "<option value=\"".$row7["id"]."\">".$row7["descricao"]." </option>";

    }   
  

?>



<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <?php echo ($title); ?>
    <!-- Custom CSS -->
    <link href="../../assets/libs/flot/css/float-chart.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <style>
     .divide {
        display:grid;
        grid-row : 1;
        grid-template-columns: 50% 50%;

     }   
     .chamconcluido {
        color:green;
        font-weight: bold;
     }
     .chamaguardando {
        color:red;
        font-weight: bold;
     }
     .chamatend{
        color:blue;
        font-weight: bold; 
     }
    </style>

</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->

    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">
        <!-- MENU LATERAL A BUSCA SUPERIOR -->
         <?php echo ($header);
               echo ($mlateral); 
         ?>

    <div class="page-wrapper">
    <div class="container-fluid">
    <div class="card">
    <div class="card-body">
    <h5 class="card-title">Chamados Desenvolvimento</h5>
    <div class="table-responsive">
        <div style="text-align:center">
<button type="button" class="btn btn-outline-info" data-toggle="modal" data-target=".bd-example-modal-lg">Abrir chamado</button>
  <button class="btn btn-outline-info" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Filtros rápidos
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="movim_desenvolvimento.php?filtro=abertos">Meus Chamados Abertos</a>
    <a class="dropdown-item" href="movim_desenvolvimento.php?filtro=desenvolvendo">Meus Chamados em Desenvolvimento</a>
    <a class="dropdown-item" href="movim_desenvolvimento.php?filtro=concluido">Meus Chamados Concluidos</a>
    <a class="dropdown-item" href="movim_desenvolvimento.php?filtro=geral">Todos os Chamados</a>
  </div>
    </div>
    <br>
<?php //echo($msg); ?>
    <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            
            <tr>
                <th>Ações</th>
                <th>ID</th>
                <th>Cliente</th>
                <th>Reponsável</th>
                <th>Sistema</th>
                <th>Status</th>
                <th>Modulo</th>
                <th>Prioridade</th>
                <th>Abertura</th>
                <th>Tempo</th>
            </tr>

        </thead>
        <tbody>
        <?php
        echo ($exibegrid);
        ?>   

        </tbody>

    </table>
</div>

</div>
</div>  
 <!--DESENHANDO NOVO CLIENTE MODAL -->
 <form class="form-horizontal" method="POST" action="movim_desenvolvimento.php?filtro=geral" target="_self">
 <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="abremodal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--CONTEUDO -->
<br>   
<h4 class="card-title">Abertura Chamados Desenvolvimento</h4>     
<div class="card-body divide">
    <div class="form-group row">
        <label for="fname" class="col-sm-3 text-right control-label col-form-label">ID</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-2" name="id" id="id" readonly="true">
        </div>
    </div>
        <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Data de abertura</label>
        <div class="col-sm-9">
            <input type="date" class="form-control col-sm-6" id="data" name="data" required>
        </div>
    </div>
  </div>
    <div class="form-group row" style="width: 90%;">
        <label for="lname" class="col-sm-1 text-left control-label col-form-label ml-4">Cliente</label>
        <div class="col-sm-9">
            <select class="form-control" name="cliente" id="cliente" style="width: 100%" required> 
                <option> </option>
                <?php
                echo $exibecli;
                ?>
            </select>
            <!--<input type="text" class="form-control" id="nome" name="nome"  required> -->
        </div>
    </div>
    <div class="divide">
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Reponsável</label>
        <div class="col-sm-9">
            <select class="form-control" name="desenv" id="desenv"> 
                <option> </option>
                <?php
                echo $exibetec;
                ?>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Módulo</label>
        <div class="col-sm-9">
            <select class="form-control" name="modulo" id="modulo"> 
                <option> </option>
                <?php
                echo $exibemod;
                ?>
            </select>
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Prioridade</label>
        <div class="col-sm-9">
            <select class="form-control" name="prioridade" id="prioridade" required>
               <option> </option> 
                <?php
                echo $exibepriori;
                ?>
            </select>
        </div>
    </div>    
    <div class="form-group row">
        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Sistema</label>
        <div class="col-sm-9">
            <select class="form-control" name="sistema" id="sistema"> 
                <option> </option>
                <?php
                echo $exibesistema;
                ?>
            </select>    
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Sprint</label>
        <div class="col-sm-9">
            <select class="form-control" name="sprint" id="sprint"> 
                <option> </option>
                <?php
                echo $exibesprint;
                ?>
            </select>
        </div>
    </div>  
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Setor</label>
        <div class="col-sm-9">
            <select class="form-control" name="setor" id="setor"> 
                <option> </option>
                <?php
                echo $exibesetor;
                ?>
            </select>
        </div>
    </div>

</div>
        <div class="form-group row" style="width: 95%">
            <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Descrição do chamado</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="descricao" id="descricao"></textarea>
            </div>
            </div>
        <input  name="operacao" type="hidden"  id="operacao" value='novo'>
        <br>

    <div class="card-body">
        <button type="submit" class="btn btn-primary">Gravar</button>
      <a href="movim_desenvolvimento.php?filtro=geral"> <button type="button" class="btn btn-primary">Sair</button></a>
    </div>
</form>

    </div>
  </div>
</div>           
</div>

 </div>
  <?php echo ($footer) ?>
    </div>


<!--DETALHAMENTO CLIENTE MODAL -->
 <form class="form-horizontal" method="POST" action="movim_desenvolvimento.php?filtro=geral" target="_self">
 <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="vermodal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--CONTEUDO -->
<br>   
<h4 class="card-title">Detalhamento do Chamado</h4>     
<div class="card-body divide">
    <div class="form-group row">
        <label for="fname" class="col-sm-3 text-right control-label col-form-label">ID</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-2" name="verid" id="verid" readonly="true">
        </div>
    </div>
        <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Data de abertura</label>
        <div class="col-sm-9">
            <input type="date" class="form-control col-sm-6" id="verdata" name="verdata" readonly="true">
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Cliente</label>
        <div class="col-sm-9">
            <select class="form-control" name="vercliente" id="vercliente" readonly="true"> 
                <option> </option>
                <?php
                echo $exibecli;
                ?>
            </select>
            <!--<input type="text" class="form-control" id="nome" name="nome"  required> -->
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Reponsável</label>
        <div class="col-sm-9">
            <select class="form-control" name="verdesenv" id="verdesenv" readonly="true"> 
                <option> </option>
                <?php
                echo $exibetec;
                ?>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Módulo</label>
        <div class="col-sm-9">
            <select class="form-control" name="vermodulo" id="vermodulo" readonly="true"> 
                <option> </option>
                <?php
                echo $exibemod;
                ?>
            </select>
        </div>
    </div> 
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Prioridade</label>
        <div class="col-sm-9">
            <select class="form-control" name="verprioridade" id="verprioridade" readonly="true">
               <option> </option> 
                <?php
                echo $exibepriori;
                ?>
            </select>
        </div>
    </div>    
    <div class="form-group row">
        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Sistema</label>
        <div class="col-sm-9">
            <select class="form-control" name="versistema" id="versistema" readonly="true"> 
                <option> </option>
                <?php
                echo $exibesistema;
                ?>
            </select>    
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Sprint</label>
        <div class="col-sm-9">
            <select class="form-control" name="versprint" id="versprint" readonly="true"> 
                <option> </option>
                <?php
                echo $exibesprint;
                ?>
            </select>
        </div>
    </div>  
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Setor</label>
        <div class="col-sm-9">
            <select class="form-control" name="versetor" id="versetor" readonly="true"> 
                <option> </option>
                <?php
                echo $exibesetor;
                ?>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Resp.Técnico</label>
        <div class="col-sm-9">
            <select class="form-control" name="vertecnico" id="vertecnico" readonly="true"> 
                <option> </option>
                <?php
                echo $exibesup;
                ?>
            </select>
        </div>
    </div>    

</div>
        <div class="form-group row" style="width: 95%">
            <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Descrição do chamado</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="verdescricao" id="verdescricao" readonly="true"></textarea>
            </div>
            </div>        
    <div class="form-group row" style="width: 95%">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Detalhamento solução do problema Suporte</label>
        <div class="col-sm-9">
            <textarea class="form-control" name="verdetalhetec" id="verdetalhetec" readonly="true"></textarea>
        </div>
     </div> 
        <div class="form-group row" style="width: 95%">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Detalhamento solução do problema Desenvolvimento</label>
        <div class="col-sm-9">
            <textarea class="form-control" name="verdetalhedev" id="verdetalhedev" readonly="true"></textarea>
        </div>
     </div>          
        <input  name="operacao" type="hidden"  id="operacao" value='novo'>
        <br>

    <div class="card-body">
      <a href="movim_desenvolvimento.php?filtro=geral"><button type="button" class="btn btn-primary" data-dismiss="modal">Sair</button></a>
    </div>
</div>
</div>
</div>
</form>

<!--ATENDIMENTO CLIENTE MODAL -->
 <form class="form-horizontal" enctype="multipart/form-data" name="atendform" method="POST" action="movim_desenvolvimento.php?filtro=geral" target="_self">
 <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="atende">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--CONTEUDO -->
<br>   
<h4 class="card-title">Atendimento Chamado</h4>   
<div style="text-align: center" id="tempo">

</div> 
<div class="card-body divide">
    <div class="form-group row">
        <label for="fname" class="col-sm-3 text-right control-label col-form-label">ID</label>
        <div class="col-sm-9">
            <input type="text" class="form-control col-sm-2" name="id" id="atid" readonly="true">
        </div>
    </div>
        <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Dt abertura</label>
        <div class="col-sm-9">
            <input type="date" class="form-control col-sm-6" id="atdata" name="data" disabled>
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Cliente</label>
        <div class="col-sm-9">
            <select class="form-control" name="cliente" id="atcliente" disabled> 
                <option> </option>
                <?php
                echo $exibecli;
                ?>
            </select>
            <!--<input type="text" class="form-control" id="nome" name="nome"  required> -->
        </div>
    </div>
    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Reponsável</label>
        <div class="col-sm-9">
            <select class="form-control" name="atdesenv" id="atdesenv" disabled> 
                <option> </option>
                <?php
                echo $exibetec;
                ?>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Módulo</label>
        <div class="col-sm-9">
            <select class="form-control" name="atmodulo" id="atmodulo" disabled> 
                <option> </option>
                <?php
                echo $exibemod;
                ?>
            </select>
        </div>
    </div> 
    <div class="form-group row">
        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Sistema</label>
        <div class="col-sm-9">
            <select class="form-control" name="sistema" id="atsistema" disabled> 
                <option> </option>
                <?php
                echo $exibesistema;
                ?>
            </select>    
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Prioridade</label>
        <div class="col-sm-9">
      <select class="form-control" name="atprioridade" id="atprioridade" disabled>
                <?php
                echo $exibepriori;
                ?>
        </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Sprint</label>
        <div class="col-sm-9">
      <select class="form-control" name="atsprint" id="atsprint" disabled>
                <?php
                echo $exibesprint;
                ?>
        </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Setor</label>
        <div class="col-sm-9">
      <select class="form-control" name="atsetor" id="atsetor" disabled>
                <?php
                echo $exibesetor;
                ?>
        </select>
        </div>
    </div>
        <div class="form-group row">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Resp.Técnico</label>
        <div class="col-sm-9">
      <select class="form-control" name="attecnico" id="attecnico" disabled>
                <?php
                echo $exibesup;
                ?>
        </select>
        </div>
    </div>
            
        <input  name="operacao1" type="hidden"  id="operacao1">
</div>
    <div class="form-group row" style="width: 95%">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Descrição do chamado</label>
        <div class="col-sm-9">
            <textarea class="form-control" name="atdescricao" id="atdescricao" disabled></textarea>
        </div>
    </div>
    <div class="form-group row" style="width: 95%">
        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Detalhamento solução do problema</label>
        <div class="col-sm-9">
            <textarea class="form-control" name="descr_dev" id="descr_dev" required></textarea>
        </div>
    </div>
    <br>
     <div style="text-align:center">
    <div class="card-body">
        <button type="submit" onclick="funcoperacao('concluir');" class="btn btn-success">Concluir</button>
        <button type="submit" onclick="funcoperacao('pausar');"class="btn btn-secondary">Pausar Chamado</button>
        <a href="movim_desenvolvimento.php?filtro=geral"> <button type="button" class="btn btn-danger" data-dismiss="modal">Sair</button></a>
    </div> 
        </div>
</div>
</div>
</div>
</form>
<input type="hidden" id="idinsert" name="idinsert">
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <script src="../../dist/js/myjs2.js"></script>
    <script src="../../dist/js/jquery.mask.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="../../assets/libs/flot/excanvas.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.pie.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.time.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.stack.js"></script>
    <script src="../../assets/libs/flot/jquery.flot.crosshair.js"></script>
    <script src="../../assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="../../dist/js/pages/chart/chart-page-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/jquery.multicheck.js"></script>
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>

        <script>
        $('#zero_config').DataTable();
        var tempoini = new Date()


           function contatempo() {

        $('#tempo').html(new Date() );
        setTimeout(contatempo,1000);

      }
    $(document).ready(function(){

        setTimeout(contatempo,1000);
        
    })

         $('#cliente').select2({
        dropdownParent: $('#abremodal'),
        placeholder: "Selecione o cliente"
    });
</script>

</body>

</html>