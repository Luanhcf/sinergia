<?php
require_once ('conexao.php');
$usr = $_SESSION ["login"];
$sql1 ="select apelido from usuarios u inner join funcionarios f on (u.funcionario=f.id) where login='$usr'";
$sqlquery1 = pg_query($conexao,$sql1);
$user = pg_fetch_assoc($sqlquery1);
$user1= $user['apelido'];


$menu = array(
 "Inicio",	
 "Cadastro",
 "Movimentação",
 "Auxiliares",
 "Relatórios",
 "Base de Conhecimento",
 "Configurações",
 "Monitoramento"
);
$subcad = array(
"Clientes",
"Funcionários",
"Usuários",
"Niveis",
"Atividade",
"Sistemas",
"Serviços Técnicos",
"Tipos de Atentimento Técnicos"
);

$submov = array(
"Atendimentos Suporte",
"Chamados Desenvolvimento",
"Treinamentos",
"Implantação",
);

$subaux = array(
"Consultar CNPJ",
"Senha suporte",
);

$subrel = array(
"Atendimento por Clientes",
"Atendimento desenvolvimento",
);

$sub_base = array(
"iCompany",
"wCompany",
"iPDV",
"xPDV",
"NF-e Monitor/ Danfe"
);

$subconf = array(
"Configurações Gerais"
);

$submonitor = array(
"Desenvolvimento",
"Suporte",
"Geral",
);

$sql = "select n.* From usuarios u inner join niveis n on (u.nivel=n.id) where trim(login)='$usr'";
$exec = pg_query($conexao,$sql);
$res = pg_fetch_assoc($exec);

$exibecadcli       = "";
$exibecadfunc      = "";
$exibecaduser      = "";
$exibecadsetor     = "";
$exibecadativ      = "";
$exibecadsist      = "";
$exibecadservtec   = "";
$exibecadatendtec  = "";
$exibeatendsup     = "";
$exibechamadev     = "";
$exibetreina       = "";
$exibeimplanta     = "";
$exibeconscnpj     = "";
$exibesenhasup     = "";

// INICIA CONTROLE DE NIVEIS DE CADASTROS
$cadcli = $res['cadcli'];
if ($cadcli != 'f'){
    $exibecadcli = "<li class=\"sidebar-item\"><a href=\"grid_cliente.php\" class=\"sidebar-link\"><i class=\"mdi mdi-note-outline\"></i><span class=\"hide-menu\"> $subcad[0] </span></a></li>";
}


$cadfunc = $res['cadfunc'];
if ($cadfunc != 'f'){
    $exibecadfunc = "<li class=\"sidebar-item\"><a href=\"grid_funcionarios.php\" class=\"sidebar-link\"><i class=\"mdi mdi-note-plus\"></i><span class=\"hide-menu\"> $subcad[1] </span></a></li>";
}


$cadusr = $res['cadusr'];
if($cadusr != 'f'){
 $exibecaduser = "<li class=\"sidebar-item\"><a href=\"grid_cad_usr.php\" class=\"sidebar-link\"><i class=\"mdi mdi-note-outline\"></i><span class=\"hide-menu\"> $subcad[2] </span></a></li>";
}

$cadsetor = $res['cadnivel'];
if ($cadsetor != 'f'){
    $exibecadsetor ="<li class=\"sidebar-item\"><a href=\"grid_niveis\" class=\"sidebar-link\"><i class=\"mdi mdi-note-outline\"></i><span class=\"hide-menu\"> $subcad[3] </span></a></li>";
}


$cadativ = $res['cadativ'];
if ($cadativ != 'f'){
 $exibecadativ = " <li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-note-outline\"></i><span class=\"hide-menu\"> $subcad[4] </span></a></li>";
}


$cadsist = $res['cadsist'];
if ($cadsist != 'f'){
$exibecadsist = "<li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-note-outline\"></i><span class=\"hide-menu\"> $subcad[5] </span></a></li>";
}


$cadservtec = $res['cadservtec'];
if ($cadservtec != 'f'){
$exibecadservtec = "<li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-note-outline\"></i><span class=\"hide-menu\"> $subcad[6] </span></a></li>";
}


$cadatendtec = $res['cadatendtec'];
if ($cadatendtec != 'f'){
 $exibecadatendtec = "<li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-note-outline\"></i><span class=\"hide-menu\"> $subcad[7] </span></a></li>";
}
// FINALIZA CONTROLE DE NIVEIS DE CADASTROS


// INICIA CONTROLE DE NIVEIS DE MOVIMENTAÇÕES
$atendsup = $res['atendsup'];
if ($atendsup != 'f'){
  $exibeatendsup =" <li class=\"sidebar-item\"><a href=\"movim_suporte.php?filtro=geral\" class=\"sidebar-link\"><i class=\"mdi mdi-emoticon\"></i><span class=\"hide-menu\"> $submov[0] </span></a></li>";
}

$atenddev = $res['atenddev'];
if ($atenddev != 'f'){
$exibechamadev ="<li class=\"sidebar-item\"><a href=\"movim_desenvolvimento.php?filtro=geral\" class=\"sidebar-link\"><i class=\"mdi mdi-emoticon-cool\"></i><span class=\"hide-menu\"> $submov[1] </span></a></li>";
}

$treina = $res['treina'];
if ($treina != 'f'){
$exibetreina = "<li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-emoticon-cool\"></i><span class=\"hide-menu\"> $submov[2] </span></a></li>";
}

$implantacao = $res['implantacao'];
if ($implantacao != 'f'){
 $exibeimplanta = "<li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-emoticon-cool\"></i><span class=\"hide-menu\"> $submov[3] </span></a></li>";
}

// FINALIZA CONTROLE DE NIVEIS DE CADASTROS

$conscnpj = $res['conscnpj'];
if ($conscnpj != 'f'){
 $exibeconscnpj = "<li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-view-dashboard\"></i><span class=\"hide-menu\"> $subaux[0] </span></a></li>";   
}

$senhasup = $res['senhasup'];
if ($senhasup != 'f'){
 $exibesenhasup = "<li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-multiplication-box\"></i><span class=\"hide-menu\"> $subaux[1] </span></a></li>";   
}


$title = "<title>Sinergia</title>";

$header = "<header class=\"topbar\" data-navbarbg=\"skin5\">
            <nav class=\"navbar top-navbar navbar-expand-md navbar-dark\">
                <div class=\"navbar-header\" data-logobg=\"skin5\">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class=\"nav-toggler waves-effect waves-light d-block d-md-none\" href=\"javascript:void(0)\"><i class=\"ti-menu ti-close\"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class=\"navbar-brand\" href=\"index.php\">
                        <!-- Logo icon -->
                        <b class=\"logo-icon p-l-10\">
                            <!--You can put here icon as well // <i class=\"wi wi-sunset\"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src=\"../../assets/images/logo-icon.png\" alt=\"\" class=\"light-logo\" />
                           
                        </b>
                        <!--End Logo icon -->
                         <!-- Logo text -->
                        <span class=\"logo-text\">
                             <!-- dark Logo text -->
                             <img src=\"../../assets/images/logo-text.png\" style=\"height:80px\" alt=\"iUniteAll\" class=\"light-logo\" />
                            
                        </span>
                    </a>
                    <a class=\"topbartoggler d-block d-md-none waves-effect waves-light\" href=\"javascript:void(0)\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"><i class=\"ti-more\"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class=\"navbar-collapse collapse\" id=\"navbarSupportedContent\" data-navbarbg=\"skin5\">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class=\"navbar-nav float-left mr-auto\">
                        <!-- ============================================================== -->
                        <!-- create new -->
                        <!-- ============================================================== -->
                        <li class=\"nav-item dropdown\">
                            <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                             <span class=\"d-block d-md-none\"><i class=\"fa fa-plus\"></i></span>   
                            </a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class=\"nav-item search-box\"> <a class=\"nav-link waves-effect waves-dark\" href=\"javascript:void(0)\"><i class=\"ti-search\"></i></a>
                            <form class=\"app-search position-absolute\">
                                <input type=\"text\" class=\"form-control\" placeholder=\"Search &amp; enter\"> <a class=\"srh-btn\"><i class=\"ti-close\"></i></a>
                            </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class=\"navbar-nav float-right\">

                        <li class=\"nav-item dropdown\">
                            <a class=\"nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic\" href=\"\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><img src=\"../../assets/images/users/foto.png\" alt=\"user\" class=\"rounded-circle\" width=\"31\"></a>
                            <div class=\"dropdown-menu dropdown-menu-right user-dd animated\">
                                <a class=\"dropdown-item\" href=\"javascript:void(0)\"><i class=\"ti-user m-r-5 m-l-5\"></i>$user1</a>
                                <div class=\"dropdown-divider\"></div>
                                <a class=\"dropdown-item\" href=\"javascript:void(0)\"><i class=\"ti-settings m-r-5 m-l-5\"></i> Configurações da conta</a>
                                <div class=\"dropdown-divider\"></div>
                                <a class=\"dropdown-item\" href=\"logout.php\"><i class=\"fa fa-power-off m-r-5 m-l-5\"></i> Logout</a>
                                <div class=\"dropdown-divider\"></div>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>";

	$mlateral = "
			 <aside class=\"left-sidebar\" data-sidebarbg=\"skin5\">
            <!-- Sidebar scroll-->
            <div class=\"scroll-sidebar\">
                <!-- Sidebar navigation-->
                <nav class=\"sidebar-nav\">
                    <ul id=\"sidebarnav\" class=\"p-t-30\">
                        <li class=\"sidebar-item\"> <a class=\"sidebar-link waves-effect waves-dark sidebar-link\" href=\"index.php\" aria-expanded=\"false\"><i class=\"mdi mdi-view-dashboard\"></i><span class=\"hide-menu\">$menu[0]</span></a></li>
                        <li class=\"sidebar-item\"> <a class=\"sidebar-link has-arrow waves-effect waves-dark\" href=\"javascript:void(0)\" aria-expanded=\"false\"><i class=\"mdi mdi-receipt\"></i><span class=\"hide-menu\">$menu[1]</span></a>
                            <ul aria-expanded=\"false\" class=\"collapse  first-level\">
                                    ".$exibecadcli."
                                    ".$exibecadfunc."
                                    ".$exibecadsist."
                                    ".$exibecadsetor."
                                    ".$exibecadativ."
                                    ".$exibecaduser."
                                    ".$exibecadservtec."
                                    ".$exibecadatendtec."
                            </ul>
                        </li>
                        <li class=\"sidebar-item\"> <a class=\"sidebar-link has-arrow waves-effect waves-dark\" href=\"javascript:void(0)\" aria-expanded=\"false\"><i class=\"mdi mdi-face\"></i><span class=\"hide-menu\">$menu[2] </span></a>
                            <ul aria-expanded=\"false\" class=\"collapse  first-level\">
                               ".$exibeatendsup."
                                ".$exibechamadev."
                                ".$exibetreina."
                                ".$exibeimplanta."
                            </ul>
                        </li>
                      
                        <li class=\"sidebar-item\"> <a class=\"sidebar-link has-arrow waves-effect waves-dark\" href=\"javascript:void(0)\" aria-expanded=\"false\"><i class=\"mdi mdi-move-resize-variant\"></i><span class=\"hide-menu\">$menu[3]</span></a>
                            <ul aria-expanded=\"false\" class=\"collapse  first-level\">
                                ".$exibeconscnpj."
                                ".$exibesenhasup." 
                            </ul>
                        </li>
                        <li class=\"sidebar-item\"> <a class=\"sidebar-link has-arrow waves-effect waves-dark\" href=\"javascript:void(0)\" aria-expanded=\"false\"><i class=\"mdi mdi-account-key\"></i><span class=\"hide-menu\">$menu[4]</span></a>
                            <ul aria-expanded=\"false\" class=\"collapse  first-level\">
                                <li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-all-inclusive\"></i><span class=\"hide-menu\"> $subrel[0] </span></a></li>
                                <li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-all-inclusive\"></i><span class=\"hide-menu\"> $subrel[1] </span></a></li>
                            </ul>
                        </li>
                        <li class=\"sidebar-item\"> <a class=\"sidebar-link has-arrow waves-effect waves-dark\" href=\"javascript:void(0)\" aria-expanded=\"false\"><i class=\"mdi mdi-alert\"></i><span class=\"hide-menu\">$menu[5]</span></a>
                            <ul aria-expanded=\"false\" class=\"collapse  first-level\">
                                <li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-alert-octagon\"></i><span class=\"hide-menu\"> $sub_base[0] </span></a></li>
                                <li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-alert-octagon\"></i><span class=\"hide-menu\"> $sub_base[1] </span></a></li>
                                <li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-alert-octagon\"></i><span class=\"hide-menu\"> $sub_base[2] </span></a></li>
                                <li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-alert-octagon\"></i><span class=\"hide-menu\"> $sub_base[3] </span></a></li>
                                <li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-alert-octagon\"></i><span class=\"hide-menu\"> $sub_base[4] </span></a></li>
                            </ul>
                        </li>
                   
                       
                        <li class=\"sidebar-item\"> <a class=\"sidebar-link has-arrow waves-effect waves-dark\" href=\"javascript:void(0)\" aria-expanded=\"false\"><i class=\"mdi mdi-alert\"></i><span class=\"hide-menu\">$menu[6]</span></a>
                            <ul aria-expanded=\"false\" class=\"collapse  first-level\">
                                <li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-alert-octagon\"></i><span class=\"hide-menu\"> $subconf[0] </span></a></li>
                            </ul>
                        </li>
                  

                        <li class=\"sidebar-item\"> <a class=\"sidebar-link has-arrow waves-effect waves-dark\" href=\"javascript:void(0)\" aria-expanded=\"false\"><i class=\"mdi mdi-alert\"></i><span class=\"hide-menu\">$menu[7]</span></a>
                            <ul aria-expanded=\"false\" class=\"collapse  first-level\">
                                <li class=\"sidebar-item\"><a href=\"monitor_dev\" class=\"sidebar-link\"><i class=\"mdi mdi-alert-octagon\"></i><span class=\"hide-menu\"> $submonitor[0] </span></a></li>
                                <li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-alert-octagon\"></i><span class=\"hide-menu\"> $submonitor[1] </span></a></li> 
                                <li class=\"sidebar-item\"><a href=\"#\" class=\"sidebar-link\"><i class=\"mdi mdi-alert-octagon\"></i><span class=\"hide-menu\"> $submonitor[2] </span></a></li>                                                               
                            </ul>
                        </li>
                     </ul>                                 
            </div>
           
        </aside>";

        $footer = "<footer class=\"footer text-center\">
                Copyright © Todos os Direitos Reservados 2020 <a href=\"http://www.nextcompany.com.br/\">Nextcompany</a>
            </footer>";

?>